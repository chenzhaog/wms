package com._520it.wms.mapper;

import com._520it.wms.domain.Role;
import com._520it.wms.query.QueryObject;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RoleMapper {
    void save(Role role);
    void update(Role role);
    void delete(Long id);
    Role get(Long id);
    Integer getCount(QueryObject qo);

    List<Role> list(QueryObject qo);

    void saveByPermission(@Param("rId") Long rId, @Param("pId") Long pId);

    List<Role> getAllRole();

    List<String> selectMyExpression(Long id);

    void deleteForPermission(Long id);

    void deleteForEmployee(Long id);

    void batchDelete(List<Long> ids);

    void saveRelationBySystemMenu(@Param("rId") Long rId, @Param("mId") Long mId);

    void deleteForMenu(Long id);

    List<String> selectByUserId(Long id);
}
