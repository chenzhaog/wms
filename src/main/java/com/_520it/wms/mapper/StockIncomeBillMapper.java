package com._520it.wms.mapper;

import com._520it.wms.domain.StockIncomeBill;
import com._520it.wms.query.StockIncomeBillQueryObject;

import java.util.List;

public interface StockIncomeBillMapper {
    int deleteByPrimaryKey(Long id);

    int insert(StockIncomeBill record);

    StockIncomeBill selectByPrimaryKey(Long id);

    List<StockIncomeBill> selectAll();

    int updateByPrimaryKey(StockIncomeBill record);
    Integer getCount(StockIncomeBillQueryObject qo);
    List<StockIncomeBill> list(StockIncomeBillQueryObject qo);

    void updateAudit(StockIncomeBill stockIncomeBill);
}