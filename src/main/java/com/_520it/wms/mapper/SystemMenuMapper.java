package com._520it.wms.mapper;

import com._520it.wms.domain.SystemMenu;
import com._520it.wms.query.SystemMenuQueryObject;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SystemMenuMapper {
    int deleteByPrimaryKey(Long id);

    int insert(SystemMenu record);

    SystemMenu selectByPrimaryKey(Long id);

    List<SystemMenu> selectAll();

    int updateByPrimaryKey(SystemMenu record);
    Integer getCount(SystemMenuQueryObject qo);
    List<SystemMenu> list(SystemMenuQueryObject qo);

    List<SystemMenu> selectAllChild();

    List<SystemMenu> queryMenusBySn(String sn);

    List<SystemMenu> queryMenusByUser(@Param("sn") String sn, @Param("id") Long id);
}