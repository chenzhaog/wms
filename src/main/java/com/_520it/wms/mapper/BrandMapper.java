package com._520it.wms.mapper;

import com._520it.wms.domain.Brand;
import com._520it.wms.query.BrandQueryObject;

import java.util.List;

public interface BrandMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Brand record);

    Brand selectByPrimaryKey(Long id);

    List<Brand> selectAll();

    int updateByPrimaryKey(Brand record);
    Integer getCount(BrandQueryObject qo);
    List<Brand> list(BrandQueryObject qo);
}