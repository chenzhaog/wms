package com._520it.wms.mapper;

import com._520it.wms.domain.StockOutcomeBillItem;
import java.util.List;

public interface StockOutcomeBillItemMapper {
    int deleteByPrimaryKey(Long id);

    int insert(StockOutcomeBillItem record);

    StockOutcomeBillItem selectByPrimaryKey(Long id);

    List<StockOutcomeBillItem> selectAll();

    int updateByPrimaryKey(StockOutcomeBillItem record);
}