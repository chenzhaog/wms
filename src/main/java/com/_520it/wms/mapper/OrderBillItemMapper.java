package com._520it.wms.mapper;

import com._520it.wms.domain.OrderBillItem;
import java.util.List;

public interface OrderBillItemMapper {
    int deleteByPrimaryKey(Long id);

    int insert(OrderBillItem record);

    OrderBillItem selectByPrimaryKey(Long id);

    List<OrderBillItem> selectAll();

    int updateByPrimaryKey(OrderBillItem record);
}