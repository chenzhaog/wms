package com._520it.wms.mapper;

import com._520it.wms.domain.StockOutcomeBill;
import com._520it.wms.query.StockOutcomeBillQueryObject;

import java.util.List;

public interface StockOutcomeBillMapper {
    int deleteByPrimaryKey(Long id);

    int insert(StockOutcomeBill record);

    StockOutcomeBill selectByPrimaryKey(Long id);

    List<StockOutcomeBill> selectAll();

    int updateByPrimaryKey(StockOutcomeBill record);
    Integer getCount(StockOutcomeBillQueryObject qo);
    List<StockOutcomeBill> list(StockOutcomeBillQueryObject qo);

    void updateAudit(StockOutcomeBill stockOutcomeBill);
}