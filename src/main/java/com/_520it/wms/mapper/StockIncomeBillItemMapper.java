package com._520it.wms.mapper;

import com._520it.wms.domain.StockIncomeBillItem;

public interface StockIncomeBillItemMapper {
    int deleteByPrimaryKey(Long id);

    int insert(StockIncomeBillItem record);

    StockIncomeBillItem selectByPrimaryKey(Long id);


}