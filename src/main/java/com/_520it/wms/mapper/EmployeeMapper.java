package com._520it.wms.mapper;

import com._520it.wms.domain.Employee;
import com._520it.wms.query.QueryObject;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface EmployeeMapper {
    void save(Employee employee);
    void update(Employee employee);
    void delete(Long id);
    Employee get(Long id);
    Integer getCount(QueryObject qo);
    List<Employee> list(QueryObject qo);

    void saveByRole(@Param("eId") Long eId, @Param("rId") Long rId);

    void deleteByRole(Long id);

    Employee getByUser(@Param("username") String username, @Param("password") String password);

    void batchDelete(List<Long> ids);

    Employee selectByUserName(String username);
}
