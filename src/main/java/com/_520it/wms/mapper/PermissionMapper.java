package com._520it.wms.mapper;

import com._520it.wms.domain.Permission;
import com._520it.wms.query.QueryObject;

import java.util.List;

public interface PermissionMapper {
    void delete(Long id);
    void save(Permission permission);
    List<String> selectForExpression();
    List<Permission> list(QueryObject qo);
    List<Permission>selectAllPermission();
    Integer getForCount();
}
