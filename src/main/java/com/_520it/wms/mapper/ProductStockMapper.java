package com._520it.wms.mapper;

import com._520it.wms.domain.ProductStock;
import com._520it.wms.query.ProductStockQueryObject;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ProductStockMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ProductStock record);

    ProductStock selectByProductIdAndDepotId(@Param("productId") Long productId, @Param("depotId") Long depotId);

    List<ProductStock> selectAll();

    int updateByPrimaryKey(ProductStock record);
    Integer getCount(ProductStockQueryObject qo);
    List<ProductStock> list(ProductStockQueryObject qo);
}