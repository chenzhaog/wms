package com._520it.wms.mapper;

import com._520it.wms.domain.Department;
import com._520it.wms.query.QueryObject;

import java.util.List;

public interface DepartmentMapper {
    void save(Department department);
    void update(Department department);
    void delete(Long id);
    Department get(Long id);
    Integer getCount(QueryObject qo);
    List<Department> list(QueryObject qo);
    List<Department> getAllName();
}
