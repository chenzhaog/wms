package com._520it.wms.service;

import com._520it.wms.domain.Department;
import com._520it.wms.query.PageResult;
import com._520it.wms.query.QueryObject;

import java.util.List;

public interface IDepartmentService {
    void save(Department department);
    void update(Department department);
    void delete(Long id);
    Department get(Long id);
    PageResult query(QueryObject qo);
    List<Department> getAllName();
}
