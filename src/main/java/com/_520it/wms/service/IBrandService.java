package com._520it.wms.service;

import com._520it.wms.domain.Brand;
import com._520it.wms.query.BrandQueryObject;
import com._520it.wms.query.PageResult;

import java.util.List;

public interface IBrandService {
    void deleteByPrimaryKey(Long id);

    void insert(Brand record);

    Brand selectByPrimaryKey(Long id);

    void updateByPrimaryKey(Brand record);
    PageResult query(BrandQueryObject qo);

    List<Brand> selectAll();

}
