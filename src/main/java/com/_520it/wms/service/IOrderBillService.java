package com._520it.wms.service;

import com._520it.wms.domain.OrderBill;
import com._520it.wms.query.OrderBillQueryObject;
import com._520it.wms.query.PageResult;

public interface IOrderBillService {

    void deleteByPrimaryKey(Long id);

    void insert(OrderBill record);

    OrderBill selectByPrimaryKey(Long id);

    void updateByPrimaryKey(OrderBill record);

    PageResult query(OrderBillQueryObject qo);

    void updateAudit(OrderBill orderBill);
}
