package com._520it.wms.service;

import com._520it.wms.domain.MenuJson;
import com._520it.wms.domain.SystemMenu;
import com._520it.wms.query.PageResult;
import com._520it.wms.query.SystemMenuQueryObject;

import java.util.List;

public interface ISystemMenuService {

    void deleteByPrimaryKey(Long id);

    void insert(SystemMenu record);

    SystemMenu selectByPrimaryKey(Long id);

    void updateByPrimaryKey(SystemMenu record);

    PageResult query(SystemMenuQueryObject qo);

    List<SystemMenu> getAllParent(Long parentId);

    List<SystemMenu> selectAll();

    List<SystemMenu> selectAllChild();

    List<MenuJson> queryMenusBySn(String sn);
}
