package com._520it.wms.service;

import com._520it.wms.domain.StockIncomeBill;
import com._520it.wms.query.PageResult;
import com._520it.wms.query.StockIncomeBillQueryObject;

public interface IStockIncomeBillService {

    void deleteByPrimaryKey(Long id);

    void insert(StockIncomeBill record);

    StockIncomeBill selectByPrimaryKey(Long id);

    void updateByPrimaryKey(StockIncomeBill record);

    PageResult query(StockIncomeBillQueryObject qo);

    void updateAudit(StockIncomeBill stockIncomeBill);
}
