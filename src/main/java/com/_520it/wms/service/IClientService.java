package com._520it.wms.service;

import com._520it.wms.domain.Client;
import com._520it.wms.query.ClientQueryObject;
import com._520it.wms.query.PageResult;

import java.util.List;

public interface IClientService {

    void deleteByPrimaryKey(Long id);

    void insert(Client record);

    Client selectByPrimaryKey(Long id);

    void updateByPrimaryKey(Client record);

    PageResult query(ClientQueryObject qo);

    List<Client> selectAll();
}
