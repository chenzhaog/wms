package com._520it.wms.service;

import com._520it.wms.query.PageResult;
import com._520it.wms.query.ProductStockQueryObject;

public interface IProductStockService {
    PageResult query(ProductStockQueryObject qo);
}
