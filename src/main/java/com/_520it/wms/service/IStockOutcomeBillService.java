package com._520it.wms.service;

import com._520it.wms.domain.StockOutcomeBill;
import com._520it.wms.query.PageResult;
import com._520it.wms.query.StockOutcomeBillQueryObject;

public interface IStockOutcomeBillService {

    void deleteByPrimaryKey(Long id);

    void insert(StockOutcomeBill record);

    StockOutcomeBill selectByPrimaryKey(Long id);

    void updateByPrimaryKey(StockOutcomeBill record);

    PageResult query(StockOutcomeBillQueryObject qo);

    void updateAudit(StockOutcomeBill stockOutcomeBill);
}
