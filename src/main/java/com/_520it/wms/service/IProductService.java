package com._520it.wms.service;

import com._520it.wms.domain.Product;
import com._520it.wms.query.PageResult;
import com._520it.wms.query.ProductQueryObject;

public interface IProductService {

    void deleteByPrimaryKey(Long id);

    void insert(Product record);

    Product selectByPrimaryKey(Long id);

    void updateByPrimaryKey(Product record);

    PageResult query(ProductQueryObject qo);
}
