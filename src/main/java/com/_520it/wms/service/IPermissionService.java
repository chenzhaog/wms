package com._520it.wms.service;

import com._520it.wms.domain.Permission;
import com._520it.wms.query.PageResult;
import com._520it.wms.query.QueryObject;

import java.util.List;

public interface IPermissionService {
    void delete(Long id);
    void save(Permission permission);
    void reload();
    List<Permission> list(QueryObject qo);
    PageResult query(QueryObject qo);
    List<Permission>selectAllPermission();
    List<String> selectForExpression();
}
