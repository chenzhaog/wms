package com._520it.wms.service;

import com._520it.wms.query.ChartQueryObject;

import java.util.List;
import java.util.Map;

public interface IChartService {
    List<Map<String,Object>> list(ChartQueryObject qo);
    List<Map<String,Object>> saleList(ChartQueryObject qo);
}
