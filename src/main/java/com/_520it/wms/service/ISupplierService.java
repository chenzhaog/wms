package com._520it.wms.service;

import com._520it.wms.domain.Supplier;
import com._520it.wms.query.PageResult;
import com._520it.wms.query.SupplierQueryObject;

import java.util.List;

public interface ISupplierService {

    void deleteByPrimaryKey(Long id);

    void insert(Supplier record);

    Supplier selectByPrimaryKey(Long id);

    void updateByPrimaryKey(Supplier record);

    PageResult query(SupplierQueryObject qo);

    List<Supplier> selectAll();

}
