package com._520it.wms.service;

import com._520it.wms.domain.Depot;
import com._520it.wms.query.DepotQueryObject;
import com._520it.wms.query.PageResult;

import java.util.List;

public interface IDepotService {

    void deleteByPrimaryKey(Long id);

    void insert(Depot record);

    Depot selectByPrimaryKey(Long id);

    void updateByPrimaryKey(Depot record);

    PageResult query(DepotQueryObject qo);

    List<Depot> selectAll();
}
