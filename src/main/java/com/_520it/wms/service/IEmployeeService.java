package com._520it.wms.service;

import com._520it.wms.domain.Employee;
import com._520it.wms.query.PageResult;
import com._520it.wms.query.QueryObject;

import java.util.List;

public interface IEmployeeService {
    void save(Employee employee);
    void update(Employee employee);
    void delete(Long id);
    Employee get(Long id);
    PageResult query(QueryObject qo);

    void getByUser(String username, String password);

    void batchDelete(List<Long> ids);

    Employee selectByUserName(String username);
}
