package com._520it.wms.service.impl;

import com._520it.wms.util.UserContext;
import com._520it.wms.domain.ProductStock;
import com._520it.wms.domain.StockIncomeBill;
import com._520it.wms.domain.StockIncomeBillItem;
import com._520it.wms.mapper.ProductStockMapper;
import com._520it.wms.mapper.StockIncomeBillItemMapper;
import com._520it.wms.mapper.StockIncomeBillMapper;
import com._520it.wms.query.PageResult;
import com._520it.wms.query.StockIncomeBillQueryObject;
import com._520it.wms.service.IStockIncomeBillService;
import lombok.Setter;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;

public class StockIncomeBillServiceImpl implements IStockIncomeBillService {
    @Setter
    private StockIncomeBillMapper stockIncomeBillMapper;
    @Setter
    private StockIncomeBillItemMapper stockIncomeBillItemMapper;
    @Setter
    private ProductStockMapper productStockMapper;
    @Override
    public void insert(StockIncomeBill stockIncomeBill) {
        BigDecimal totalNumber=BigDecimal.ZERO;
        BigDecimal totalAmount=BigDecimal.ZERO;
        stockIncomeBill.setInputTime(new Date());
        stockIncomeBill.setInputUser(UserContext.getSessionUser());
        stockIncomeBill.setStatus(StockIncomeBill.NORMAL);
        if(stockIncomeBill.getItems().size()>0){
            for (StockIncomeBillItem item : stockIncomeBill.getItems()) {
                BigDecimal number = item.getNumber();
                BigDecimal price = item.getCostPrice();
                BigDecimal amount = number.multiply(price).setScale(2, RoundingMode.HALF_UP);
                item.setAmount(amount);
                totalAmount=totalAmount.add(amount);
                totalNumber=totalNumber.add(number);
            }
        }
        stockIncomeBill.setTotalAmount(totalAmount);
        stockIncomeBill.setTotalNumber(totalNumber);
        stockIncomeBillMapper.insert(stockIncomeBill);
        for (StockIncomeBillItem item : stockIncomeBill.getItems()) {
            item.setBillId(stockIncomeBill.getId());
            stockIncomeBillItemMapper.insert(item);
        }
    }

    @Override
    public void updateByPrimaryKey(StockIncomeBill stockIncomeBill) {
        stockIncomeBillItemMapper.deleteByPrimaryKey(stockIncomeBill.getId());
        BigDecimal totalNumber=BigDecimal.ZERO;
        BigDecimal totalAmount=BigDecimal.ZERO;
        if(stockIncomeBill.getItems().size()>0){
            for (StockIncomeBillItem item : stockIncomeBill.getItems()) {
                BigDecimal number = item.getNumber();
                BigDecimal price = item.getCostPrice();
                BigDecimal amount = number.multiply(price).setScale(2, RoundingMode.HALF_UP);
                item.setAmount(amount);
                totalAmount=totalAmount.add(amount);
                totalNumber=totalNumber.add(number);
            }
        }
        stockIncomeBill.setTotalAmount(totalAmount);
        stockIncomeBill.setTotalNumber(totalNumber);
        stockIncomeBillMapper.updateByPrimaryKey(stockIncomeBill);
        for (StockIncomeBillItem item : stockIncomeBill.getItems()) {
            item.setBillId(stockIncomeBill.getId());
            stockIncomeBillItemMapper.insert(item);
        }
    }

    @Override
    public void deleteByPrimaryKey(Long id) {
        stockIncomeBillItemMapper.deleteByPrimaryKey(id);
        stockIncomeBillMapper.deleteByPrimaryKey(id);
    }

    @Override
    public StockIncomeBill selectByPrimaryKey(Long id) {
        return stockIncomeBillMapper.selectByPrimaryKey(id);
    }

    @Override
    public PageResult query(StockIncomeBillQueryObject qo) {
        Integer totalCount = stockIncomeBillMapper.getCount(qo);
        if (totalCount < 1) {
            return PageResult.EMPTY;
        }
        List<StockIncomeBill> list = stockIncomeBillMapper.list(qo);
        return new PageResult(qo.getCurrentPage(), qo.getPageSize(), list, totalCount);
    }

    @Override
    public void updateAudit(StockIncomeBill stockIncomeBill) {
        stockIncomeBill = stockIncomeBillMapper.selectByPrimaryKey(stockIncomeBill.getId());
        if(stockIncomeBill.getStatus()==0){
            stockIncomeBill.setAuditTime(new Date());
            stockIncomeBill.setAuditor(UserContext.getSessionUser());
            stockIncomeBill.setStatus(StockIncomeBill.AUDIT);
            stockIncomeBillMapper.updateAudit(stockIncomeBill);
            for (StockIncomeBillItem item : stockIncomeBill.getItems()) {
                ProductStock productStock = productStockMapper.selectByProductIdAndDepotId(item.getProduct().getId(), stockIncomeBill.getDepot().getId());
                if(productStock!=null){
                    productStock.setStoreNumber(productStock.getStoreNumber().add(item.getNumber()));
                    productStock.setAmount(productStock.getAmount().add(item.getAmount()));
                    productStock.setPrice(productStock.getAmount().divide(productStock.getStoreNumber(),2,RoundingMode.HALF_UP));
                    productStockMapper.updateByPrimaryKey(productStock);
                }else {
                    productStock=new ProductStock();
                    productStock.setAmount(item.getAmount());
                    productStock.setStoreNumber(item.getNumber());
                    productStock.setPrice(item.getCostPrice());
                    productStock.setDepot(stockIncomeBill.getDepot());
                    productStock.setProduct(item.getProduct());
                    productStock.setIncomeDate(new Date());
                    productStockMapper.insert(productStock);
                }
            }
        }else {
            throw new RuntimeException("审核出错");
        }

    }
}