package com._520it.wms.service.impl;

import com._520it.wms.util.UserContext;
import com._520it.wms.domain.*;
import com._520it.wms.mapper.ProductStockMapper;
import com._520it.wms.mapper.SaleAccountMapper;
import com._520it.wms.mapper.StockOutcomeBillItemMapper;
import com._520it.wms.mapper.StockOutcomeBillMapper;
import com._520it.wms.query.PageResult;
import com._520it.wms.query.StockOutcomeBillQueryObject;
import com._520it.wms.service.IStockOutcomeBillService;
import lombok.Setter;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;

public class StockOutcomeBillServiceImpl implements IStockOutcomeBillService {
    @Setter
    private StockOutcomeBillMapper stockOutcomeBillMapper;
    @Setter
    private StockOutcomeBillItemMapper stockOutcomeBillItemMapper;
    @Setter
    private ProductStockMapper productStockMapper;
    @Setter
    private SaleAccountMapper saleAccountMapper;
    @Override
    public void insert(StockOutcomeBill stockOutcomeBill) {
        BigDecimal totalNumber=BigDecimal.ZERO;
        BigDecimal totalAmount=BigDecimal.ZERO;
        stockOutcomeBill.setInputTime(new Date());
        stockOutcomeBill.setInputUser(UserContext.getSessionUser());
        stockOutcomeBill.setStatus(StockOutcomeBill.NORMAL);
        if(stockOutcomeBill.getItems().size()>0){
            for (StockOutcomeBillItem item : stockOutcomeBill.getItems()) {
                BigDecimal number = item.getNumber();
                BigDecimal price = item.getSalePrice();
                BigDecimal amount = number.multiply(price).setScale(2, RoundingMode.HALF_UP);
                item.setAmount(amount);
                totalAmount=totalAmount.add(amount);
                totalNumber=totalNumber.add(number);
            }
        }
        stockOutcomeBill.setTotalAmount(totalAmount);
        stockOutcomeBill.setTotalNumber(totalNumber);
        stockOutcomeBillMapper.insert(stockOutcomeBill);
        for (StockOutcomeBillItem item : stockOutcomeBill.getItems()) {
            item.setBillId(stockOutcomeBill.getId());
            stockOutcomeBillItemMapper.insert(item);
        }
    }

    @Override
    public void updateByPrimaryKey(StockOutcomeBill stockOutcomeBill) {
        stockOutcomeBillItemMapper.deleteByPrimaryKey(stockOutcomeBill.getId());
        BigDecimal totalNumber=BigDecimal.ZERO;
        BigDecimal totalAmount=BigDecimal.ZERO;
        if(stockOutcomeBill.getItems().size()>0){
            for (StockOutcomeBillItem item : stockOutcomeBill.getItems()) {
                BigDecimal number = item.getNumber();
                BigDecimal price = item.getSalePrice();
                BigDecimal amount = number.multiply(price).setScale(2, RoundingMode.HALF_UP);
                item.setAmount(amount);
                totalAmount=totalAmount.add(amount);
                totalNumber=totalNumber.add(number);
            }
        }
        stockOutcomeBill.setTotalAmount(totalAmount);
        stockOutcomeBill.setTotalNumber(totalNumber);
        stockOutcomeBillMapper.updateByPrimaryKey(stockOutcomeBill);
        for (StockOutcomeBillItem item : stockOutcomeBill.getItems()) {
            item.setBillId(stockOutcomeBill.getId());
            stockOutcomeBillItemMapper.insert(item);
        }
    }

    @Override
    public void deleteByPrimaryKey(Long id) {
        stockOutcomeBillItemMapper.deleteByPrimaryKey(id);
        stockOutcomeBillMapper.deleteByPrimaryKey(id);
    }

    @Override
    public StockOutcomeBill selectByPrimaryKey(Long id) {
        return stockOutcomeBillMapper.selectByPrimaryKey(id);
    }

    @Override
    public PageResult query(StockOutcomeBillQueryObject qo) {
        Integer totalCount = stockOutcomeBillMapper.getCount(qo);
        if (totalCount < 1) {
            return PageResult.EMPTY;
        }
        List<StockOutcomeBill> list = stockOutcomeBillMapper.list(qo);
        return new PageResult(qo.getCurrentPage(), qo.getPageSize(), list, totalCount);
    }

    @Override
    public void updateAudit(StockOutcomeBill stockOutcomeBill) {
        stockOutcomeBill = stockOutcomeBillMapper.selectByPrimaryKey(stockOutcomeBill.getId());
        if(stockOutcomeBill.getStatus()==0){
            stockOutcomeBill.setAuditTime(new Date());
            stockOutcomeBill.setAuditor(UserContext.getSessionUser());
            stockOutcomeBill.setStatus(StockOutcomeBill.AUDIT);
            stockOutcomeBillMapper.updateAudit(stockOutcomeBill);
            for (StockOutcomeBillItem item : stockOutcomeBill.getItems()) {
                ProductStock productStock = productStockMapper.selectByProductIdAndDepotId(item.getProduct().getId(), stockOutcomeBill.getDepot().getId());
                if(productStock!=null){
                    BigDecimal number = productStock.getStoreNumber().subtract(item.getNumber());
                    if(number.compareTo(BigDecimal.ZERO)==-1){
                        throw new RuntimeException("库存不足");
                    }
                    productStock.setStoreNumber(number);
                    productStock.setAmount(productStock.getPrice().multiply(number).setScale(2,RoundingMode.HALF_UP));
                    productStock.setOutcomeDate(new Date());
                    productStockMapper.updateByPrimaryKey(productStock);
                    SaleAccount sale=new SaleAccount();
                    sale.setClientId(stockOutcomeBill.getClient().getId());
                    sale.setCostPrice(productStock.getPrice());
                    sale.setNumber(item.getNumber());
                    sale.setSaleManId(stockOutcomeBill.getInputUser().getId());
                    sale.setProductId(item.getProduct().getId());
                    sale.setVdate(stockOutcomeBill.getVdate());
                    sale.setSalePrice(item.getSalePrice());
                    sale.setCostAmount(sale.getNumber().multiply(sale.getCostPrice()).setScale(2,RoundingMode.HALF_UP));
                    sale.setSaleAmount(sale.getNumber().multiply(sale.getSalePrice()).setScale(2,RoundingMode.HALF_UP));
                    saleAccountMapper.insert(sale);
                }else {
                    throw new RuntimeException("不存在该商品");
                }
            }
        }else {
            throw new RuntimeException("审核出错");
        }
    }
}