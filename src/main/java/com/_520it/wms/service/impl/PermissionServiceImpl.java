package com._520it.wms.service.impl;

import com._520it.wms.domain.Permission;
import com._520it.wms.mapper.PermissionMapper;
import com._520it.wms.query.PageResult;
import com._520it.wms.query.QueryObject;
import com._520it.wms.service.IPermissionService;
import com._520it.wms.web.action.BaseAction;
import lombok.Setter;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

public class PermissionServiceImpl implements IPermissionService, ApplicationContextAware {
    @Setter
    private PermissionMapper permissionMapper;
    private ApplicationContext ctx;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.ctx = applicationContext;
    }

    @Override
    public void delete(Long id) {
        permissionMapper.delete(id);
    }

    @Override
    public void save(Permission permission) {
        permissionMapper.save(permission);
    }

    public List<String> selectForExpression() {
        return permissionMapper.selectForExpression();
    }

    @Override
    public void reload() {
        Permission permission = new Permission();
        List<String> expression = permissionMapper.selectForExpression();
        Map<String, BaseAction> beansOfType = ctx.getBeansOfType(BaseAction.class);
        for (BaseAction baseAction : beansOfType.values()) {
            for (Method method : baseAction.getClass().getDeclaredMethods()) {
                System.out.println(method);
            /*    String className = baseAction.getClass().getSimpleName();
                String methodName = method.getName();
                String name = className + ":" + methodName;*/
                RequiresPermissions an1 = method.getAnnotation(RequiresPermissions.class);
                if (an1 != null) {
                    if (!expression.contains(an1.value()[0])) {
                        permission.setName(an1.value()[0]);
                        permission.setExpression(an1.value()[1]);
                        permissionMapper.save(permission);
                    }
                }
            }
        }
    }

    @Override
    public List<Permission> list(QueryObject qo) {
        return permissionMapper.list(qo);
    }

    @Override
    public PageResult query(QueryObject qo) {
        Integer totalCount = permissionMapper.getForCount();
        if (totalCount < 1) {
            return PageResult.EMPTY;
        }
        List<Permission> list = list(qo);
        return new PageResult(qo.getCurrentPage(), qo.getPageSize(), list, totalCount);
    }

    @Override
    public List<Permission> selectAllPermission() {
        return permissionMapper.selectAllPermission();
    }

}
