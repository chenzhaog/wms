package com._520it.wms.service.impl;

import com._520it.wms.domain.Client;
import com._520it.wms.mapper.ClientMapper;
import com._520it.wms.query.ClientQueryObject;
import com._520it.wms.query.PageResult;
import com._520it.wms.service.IClientService;
import lombok.Setter;

import java.util.List;

public class ClientServiceImpl implements IClientService {
    @Setter
    private ClientMapper clientMapper;

    @Override
    public void insert(Client client) {
        clientMapper.insert(client);
    }

    @Override
    public void updateByPrimaryKey(Client client) {
        clientMapper.updateByPrimaryKey(client);
    }

    @Override
    public void deleteByPrimaryKey(Long id) {
        clientMapper.deleteByPrimaryKey(id);
    }

    @Override
    public Client selectByPrimaryKey(Long id) {
        return clientMapper.selectByPrimaryKey(id);
    }

    @Override
    public PageResult query(ClientQueryObject qo) {
        Integer totalCount = clientMapper.getCount(qo);
        if (totalCount < 1) {
            return PageResult.EMPTY;
        }
        List<Client> list = clientMapper.list(qo);
        return new PageResult(qo.getCurrentPage(), qo.getPageSize(), list, totalCount);
    }

    @Override
    public List<Client> selectAll() {
        return clientMapper.selectAll();
    }
}