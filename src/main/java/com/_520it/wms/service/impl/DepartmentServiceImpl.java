package com._520it.wms.service.impl;

import com._520it.wms.domain.Department;
import com._520it.wms.mapper.DepartmentMapper;
import com._520it.wms.query.PageResult;
import com._520it.wms.query.QueryObject;
import com._520it.wms.service.IDepartmentService;
import lombok.Setter;

import java.util.List;


public class DepartmentServiceImpl implements IDepartmentService {
    @Setter
    private DepartmentMapper departmentMapper;
    @Override
    public void save(Department department) {
        departmentMapper.save(department);
    }
    @Override
    public void update(Department department) {
        departmentMapper.update(department);
    }

    @Override
    public void delete(Long id) {
        departmentMapper.delete(id);
    }

    @Override
    public Department get(Long id) {
        return departmentMapper.get(id);
    }

    @Override
    public PageResult query(QueryObject qo) {
        Integer totalCount=departmentMapper.getCount(qo);
        if(totalCount<1){
            return PageResult.EMPTY;
        }
        List<Department> list=departmentMapper.list(qo);
        return new PageResult(qo.getCurrentPage(),qo.getPageSize(),list,totalCount);
    }

    @Override
    public List<Department> getAllName() {
        return departmentMapper.getAllName();
    }
}