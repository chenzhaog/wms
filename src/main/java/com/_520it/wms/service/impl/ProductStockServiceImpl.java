package com._520it.wms.service.impl;

import com._520it.wms.domain.ProductStock;
import com._520it.wms.mapper.ProductStockMapper;
import com._520it.wms.query.PageResult;
import com._520it.wms.query.ProductStockQueryObject;
import com._520it.wms.service.IProductStockService;
import lombok.Setter;

import java.util.List;

public class ProductStockServiceImpl implements IProductStockService {
    @Setter
    private ProductStockMapper productStockMapper;
    @Override
    public PageResult query(ProductStockQueryObject qo) {
        Integer totalCount = productStockMapper.getCount(qo);
        if (totalCount < 1) {
            return PageResult.EMPTY;
        }
        List<ProductStock> list = productStockMapper.list(qo);
        return new PageResult(qo.getCurrentPage(), qo.getPageSize(), list, totalCount);
    }
}