package com._520it.wms.service.impl;

import com._520it.wms.domain.Product;
import com._520it.wms.mapper.ProductMapper;
import com._520it.wms.query.PageResult;
import com._520it.wms.query.ProductQueryObject;
import com._520it.wms.service.IProductService;
import lombok.Setter;

import java.util.List;

public class ProductServiceImpl implements IProductService {
    @Setter
    private ProductMapper productMapper;

    @Override
    public void insert(Product product) {
        productMapper.insert(product);
    }

    @Override
    public void updateByPrimaryKey(Product product) {
        productMapper.updateByPrimaryKey(product);
    }

    @Override
    public void deleteByPrimaryKey(Long id) {
        productMapper.deleteByPrimaryKey(id);
    }

    @Override
    public Product selectByPrimaryKey(Long id) {
        return productMapper.selectByPrimaryKey(id);
    }

    @Override
    public PageResult query(ProductQueryObject qo) {
        Integer totalCount = productMapper.getCount(qo);
        if (totalCount < 1) {
            return PageResult.EMPTY;
        }
        List<Product> list = productMapper.list(qo);
        return new PageResult(qo.getCurrentPage(), qo.getPageSize(), list, totalCount);
    }
}