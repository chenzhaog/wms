package com._520it.wms.service.impl;

import com._520it.wms.domain.Brand;
import com._520it.wms.mapper.BrandMapper;
import com._520it.wms.query.BrandQueryObject;
import com._520it.wms.query.PageResult;
import com._520it.wms.service.IBrandService;
import lombok.Setter;

import java.util.List;

public class BrandServiceImpl implements IBrandService {
    @Setter
    private BrandMapper brandMapper;
    @Override
    public void insert(Brand brand) {
        brandMapper.insert(brand);
    }
    @Override
    public void updateByPrimaryKey(Brand brand) {
        brandMapper.updateByPrimaryKey(brand);
    }

    @Override
    public void deleteByPrimaryKey(Long id) {
        brandMapper.deleteByPrimaryKey(id);
    }

    @Override
    public Brand selectByPrimaryKey(Long id) {
        return brandMapper.selectByPrimaryKey(id);
    }

    @Override
    public PageResult query(BrandQueryObject qo) {
        Integer totalCount=brandMapper.getCount(qo);
        if(totalCount<1){
            return PageResult.EMPTY;
        }
        List<Brand> list=brandMapper.list(qo);
        return new PageResult(qo.getCurrentPage(),qo.getPageSize(),list,totalCount);
    }

    @Override
    public List<Brand> selectAll() {
        return brandMapper.selectAll();
    }
}
