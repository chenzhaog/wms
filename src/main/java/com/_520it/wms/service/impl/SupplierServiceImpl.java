package com._520it.wms.service.impl;

import com._520it.wms.domain.Supplier;
import com._520it.wms.mapper.SupplierMapper;
import com._520it.wms.query.PageResult;
import com._520it.wms.query.SupplierQueryObject;
import com._520it.wms.service.ISupplierService;
import lombok.Setter;

import java.util.List;

public class SupplierServiceImpl implements ISupplierService {
    @Setter
    private SupplierMapper supplierMapper;

    @Override
    public void insert(Supplier supplier) {
        supplierMapper.insert(supplier);
    }

    @Override
    public void updateByPrimaryKey(Supplier supplier) {
        supplierMapper.updateByPrimaryKey(supplier);
    }

    @Override
    public void deleteByPrimaryKey(Long id) {
        supplierMapper.deleteByPrimaryKey(id);
    }

    @Override
    public Supplier selectByPrimaryKey(Long id) {
        return supplierMapper.selectByPrimaryKey(id);
    }

    @Override
    public PageResult query(SupplierQueryObject qo) {
        Integer totalCount = supplierMapper.getCount(qo);
        if (totalCount < 1) {
            return PageResult.EMPTY;
        }
        List<Supplier> list = supplierMapper.list(qo);
        return new PageResult(qo.getCurrentPage(), qo.getPageSize(), list, totalCount);
    }

    @Override
    public List<Supplier> selectAll() {
        return supplierMapper.selectAll();
    }
}