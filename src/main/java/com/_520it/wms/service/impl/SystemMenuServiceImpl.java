package com._520it.wms.service.impl;

import com._520it.wms.util.UserContext;
import com._520it.wms.domain.Employee;
import com._520it.wms.domain.MenuJson;
import com._520it.wms.domain.SystemMenu;
import com._520it.wms.mapper.SystemMenuMapper;
import com._520it.wms.query.PageResult;
import com._520it.wms.query.SystemMenuQueryObject;
import com._520it.wms.service.ISystemMenuService;
import lombok.Setter;

import java.util.*;

public class SystemMenuServiceImpl implements ISystemMenuService {
    @Setter
    private SystemMenuMapper systemMenuMapper;

    @Override
    public void insert(SystemMenu systemMenu) {
        systemMenuMapper.insert(systemMenu);
    }

    @Override
    public void updateByPrimaryKey(SystemMenu systemMenu) {
        systemMenuMapper.updateByPrimaryKey(systemMenu);
    }

    @Override
    public void deleteByPrimaryKey(Long id) {
        systemMenuMapper.deleteByPrimaryKey(id);
    }

    @Override
    public SystemMenu selectByPrimaryKey(Long id) {
        return systemMenuMapper.selectByPrimaryKey(id);
    }

    @Override
    public PageResult query(SystemMenuQueryObject qo) {
        Integer totalCount = systemMenuMapper.getCount(qo);
        if (totalCount < 1) {
            return PageResult.EMPTY;
        }
        List<SystemMenu> list = systemMenuMapper.list(qo);
        return new PageResult(qo.getCurrentPage(), qo.getPageSize(), list, totalCount);
    }

    @Override
    public List<SystemMenu> getAllParent(Long parentId) {
        List<SystemMenu> parentList = new ArrayList<>();
        SystemMenu parentMenu = systemMenuMapper.selectByPrimaryKey(parentId);
        parentList.add(parentMenu);
        while (parentMenu.getParent() != null) {
            parentMenu = systemMenuMapper.selectByPrimaryKey(parentMenu.getParent().getId());
            parentList.add(parentMenu);
        }
        Collections.reverse(parentList);
        return parentList;
    }

    @Override
    public List<SystemMenu> selectAll() {
        return systemMenuMapper.selectAll();
    }

    @Override
    public List<SystemMenu> selectAllChild() {
        return systemMenuMapper.selectAllChild();
    }

    @Override
    public List<MenuJson> queryMenusBySn(String sn) {
        Employee user = UserContext.getSessionUser();
        List<MenuJson> menus = new ArrayList<>();
        List<SystemMenu> list = new ArrayList<>();
        if (user.isAdmin()) {
            list = systemMenuMapper.queryMenusBySn(sn);
        } else {
            list = systemMenuMapper.queryMenusByUser(sn, user.getId());
        }
        for (SystemMenu sm : list) {
            MenuJson mj = new MenuJson();
            mj.setId(sm.getId());
//            mj.setPid(sm.getParent().getId());
            mj.setUrl(sm.getUrl() + ".action");
            mj.setName(sm.getName());
            menus.add(mj);
        }

        return menus;
    }
}