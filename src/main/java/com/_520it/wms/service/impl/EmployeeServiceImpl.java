package com._520it.wms.service.impl;

import com._520it.wms.util.MD5;
import com._520it.wms.util.UserContext;
import com._520it.wms.domain.Employee;
import com._520it.wms.domain.Role;
import com._520it.wms.mapper.EmployeeMapper;
import com._520it.wms.query.PageResult;
import com._520it.wms.query.QueryObject;
import com._520it.wms.service.IEmployeeService;
import com._520it.wms.service.IRoleService;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

public class EmployeeServiceImpl implements IEmployeeService {
    @Setter
    private EmployeeMapper employeeMapper;
    @Setter
    private IRoleService roleService;
    public void save(Employee employee) {
        employee.setPassword(MD5.encode(employee.getPassword()));
        employeeMapper.save(employee);
        Long eId = employee.getId();
        for (Role role : employee.getRoles()) {
            Long rId = role.getId();
            employeeMapper.saveByRole(eId, rId);
        }
    }

    public void update(Employee employee) {
        employeeMapper.update(employee);
        Long eId = employee.getId();
        employeeMapper.deleteByRole(eId);
        for (Role role : employee.getRoles()) {
            Long rId = role.getId();
            employeeMapper.saveByRole(eId, rId);
        }
    }

    public void delete(Long id) {
        employeeMapper.delete(id);
        employeeMapper.deleteByRole(id);
    }

    public Employee get(Long id) {
        return employeeMapper.get(id);
    }

    public PageResult query(QueryObject qo) {
        Integer totalCount = employeeMapper.getCount(qo);
        if (totalCount < 1) {
            return PageResult.EMPTY;
        }
        List<Employee> list = employeeMapper.list(qo);
        System.out.println(qo.getCurrentPage());
        return new PageResult(qo.getCurrentPage(), qo.getPageSize(), list, totalCount);
    }

    public void getByUser(String username, String password) {
        Employee employee = employeeMapper.getByUser(username, password);
        if (employee == null) {
            throw new RuntimeException("用户或密码错误");
        }
        List<String> expressions=new ArrayList<>();
        UserContext.putContextUser(employee);
        for (Role role : employee.getRoles()) {
            List<String> expression =roleService.selectMyExpression(role.getId());
            for (String s : expression) {
                expressions.add(s);
            }
        }
        UserContext.putContextExpression(expressions);
    }

    @Override
    public void batchDelete(List<Long> ids) {
        System.out.println(ids);
        employeeMapper.batchDelete(ids);
    }

    @Override
    public Employee selectByUserName(String username) {
        return employeeMapper.selectByUserName(username);
    }
}
