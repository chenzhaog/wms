package com._520it.wms.service.impl;

import com._520it.wms.mapper.ChartMapper;
import com._520it.wms.query.ChartQueryObject;
import com._520it.wms.service.IChartService;
import lombok.Setter;

import java.util.List;
import java.util.Map;

public class ChartServiceImpl implements IChartService {
    @Setter
    private ChartMapper chartMapper;

    @Override
    public List<Map<String, Object>> list(ChartQueryObject qo) {
        return chartMapper.list(qo);
    }

    @Override
    public List<Map<String, Object>> saleList(ChartQueryObject qo) {
        return chartMapper.saleList(qo);
    }
}