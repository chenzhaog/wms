package com._520it.wms.service.impl;

import com._520it.wms.domain.Depot;
import com._520it.wms.mapper.DepotMapper;
import com._520it.wms.query.DepotQueryObject;
import com._520it.wms.query.PageResult;
import com._520it.wms.service.IDepotService;
import lombok.Setter;

import java.util.List;

public class DepotServiceImpl implements IDepotService {
    @Setter
    private DepotMapper depotMapper;

    @Override
    public void insert(Depot depot) {
        depotMapper.insert(depot);
    }

    @Override
    public void updateByPrimaryKey(Depot depot) {
        depotMapper.updateByPrimaryKey(depot);
    }

    @Override
    public void deleteByPrimaryKey(Long id) {
        depotMapper.deleteByPrimaryKey(id);
    }

    @Override
    public Depot selectByPrimaryKey(Long id) {
        return depotMapper.selectByPrimaryKey(id);
    }

    @Override
    public PageResult query(DepotQueryObject qo) {
        Integer totalCount = depotMapper.getCount(qo);
        if (totalCount < 1) {
            return PageResult.EMPTY;
        }
        List<Depot> list = depotMapper.list(qo);
        return new PageResult(qo.getCurrentPage(), qo.getPageSize(), list, totalCount);
    }

    @Override
    public List<Depot> selectAll() {
        return depotMapper.selectAll();
    }
}