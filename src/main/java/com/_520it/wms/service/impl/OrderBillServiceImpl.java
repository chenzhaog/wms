package com._520it.wms.service.impl;

import com._520it.wms.util.UserContext;
import com._520it.wms.domain.OrderBill;
import com._520it.wms.domain.OrderBillItem;
import com._520it.wms.mapper.OrderBillItemMapper;
import com._520it.wms.mapper.OrderBillMapper;
import com._520it.wms.query.OrderBillQueryObject;
import com._520it.wms.query.PageResult;
import com._520it.wms.service.IOrderBillService;
import lombok.Setter;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;

public class OrderBillServiceImpl implements IOrderBillService {
    @Setter
    private OrderBillMapper orderBillMapper;
    @Setter
    private OrderBillItemMapper orderBillItemMapper;
    @Override
    public void insert(OrderBill orderBill) {
        System.out.println(orderBill);
        BigDecimal totalAmount=BigDecimal.ZERO;
        BigDecimal totalNumber=BigDecimal.ZERO;
        for (OrderBillItem item : orderBill.getItems()) {
            item.setAmount(BigDecimal.ZERO);
            BigDecimal number = item.getNumber();
            BigDecimal amount = number.multiply(item.getCostPrice()).setScale(2, RoundingMode.HALF_UP);
            item.setAmount(amount);
            totalAmount=totalAmount.add(amount);
            totalNumber=totalNumber.add(number);
        }
        orderBill.setTotalAmount(totalAmount);
        orderBill.setTotalNumber(totalNumber);
        orderBill.setInputTime(new Date());
        orderBill.setInputUser(UserContext.getSessionUser());
        orderBill.setStatus(OrderBill.NORMAL);
        orderBillMapper.insert(orderBill);
        Long orderBillId = orderBill.getId();
        for (OrderBillItem item : orderBill.getItems()) {
            item.setAmount(BigDecimal.ZERO);
            BigDecimal number = item.getNumber();
            BigDecimal amount = number.multiply(item.getCostPrice()).setScale(2, RoundingMode.HALF_UP);
            item.setAmount(amount);
            item.setOrderBillId(orderBillId);
            orderBillItemMapper.insert(item);
        }

    }

    @Override
    public void updateByPrimaryKey(OrderBill orderBill) {
        orderBillItemMapper.deleteByPrimaryKey(orderBill.getId());
        BigDecimal totalAmount=BigDecimal.ZERO;
        BigDecimal totalNumber=BigDecimal.ZERO;
        for (OrderBillItem item : orderBill.getItems()) {
            item.setAmount(BigDecimal.ZERO);
            BigDecimal number = item.getNumber();
            BigDecimal amount = number.multiply(item.getCostPrice()).setScale(2, RoundingMode.HALF_UP);
            item.setAmount(amount);
            totalAmount=totalAmount.add(amount);
            totalNumber=totalNumber.add(number);
        }
        orderBill.setTotalAmount(totalAmount);
        orderBill.setTotalNumber(totalNumber);
        orderBill.setInputTime(new Date());
        orderBill.setInputUser(UserContext.getSessionUser());
        orderBill.setStatus(OrderBill.NORMAL);
        orderBillMapper.updateByPrimaryKey(orderBill);
        Long orderBillId = orderBill.getId();
        for (OrderBillItem item : orderBill.getItems()) {
            item.setAmount(BigDecimal.ZERO);
            BigDecimal number = item.getNumber();
            BigDecimal amount = number.multiply(item.getCostPrice()).setScale(2, RoundingMode.HALF_UP);
            item.setAmount(amount);
            item.setOrderBillId(orderBillId);
            orderBillItemMapper.insert(item);
        }

    }

    @Override
    public void deleteByPrimaryKey(Long id) {
        orderBillItemMapper.deleteByPrimaryKey(id);
        orderBillMapper.deleteByPrimaryKey(id);
    }

    @Override
    public OrderBill selectByPrimaryKey(Long id) {
        return orderBillMapper.selectByPrimaryKey(id);
    }

    @Override
    public PageResult query(OrderBillQueryObject qo) {
        Integer totalCount = orderBillMapper.getCount(qo);
        if (totalCount < 1) {
            return PageResult.EMPTY;
        }
        List<OrderBill> list = orderBillMapper.list(qo);
        return new PageResult(qo.getCurrentPage(), qo.getPageSize(), list, totalCount);
    }

    @Override
    public void updateAudit(OrderBill orderBill) {
        orderBill.setStatus(OrderBill.AUDIT);
        orderBill.setAuditTime(new Date());
        orderBill.setAuditor(UserContext.getSessionUser());
        orderBillMapper.updateAudit(orderBill);
    }
}