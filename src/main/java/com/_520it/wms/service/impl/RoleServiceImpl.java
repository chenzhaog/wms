package com._520it.wms.service.impl;

import com._520it.wms.domain.Permission;
import com._520it.wms.domain.Role;
import com._520it.wms.domain.SystemMenu;
import com._520it.wms.mapper.RoleMapper;
import com._520it.wms.query.PageResult;
import com._520it.wms.query.QueryObject;
import com._520it.wms.service.IRoleService;
import lombok.Setter;

import java.util.List;


public class RoleServiceImpl implements IRoleService {
    @Setter
    private RoleMapper roleMapper;
    @Override
    public void save(Role role) {
        roleMapper.save(role);
        Long rId=role.getId();
        for (Permission permission : role.getPermissions()) {
            Long pId=permission.getId();
            roleMapper.saveByPermission(rId,pId);
        }
        for (SystemMenu menu : role.getMenus()) {
            Long mId = menu.getId();
            roleMapper.saveRelationBySystemMenu(rId,mId);
        }
    }
    @Override
    public void update(Role role) {
        roleMapper.update(role);
        Long rId=role.getId();
        roleMapper.deleteForPermission(rId);
        for (Permission permission : role.getPermissions()) {
            Long pId=permission.getId();
            roleMapper.saveByPermission(rId,pId);
        }
        roleMapper.deleteForMenu(rId);
        for (SystemMenu menu : role.getMenus()) {
            Long mId = menu.getId();
            roleMapper.saveRelationBySystemMenu(rId,mId);
        }

    }

    @Override
    public void delete(Long id) {
        roleMapper.delete(id);
        roleMapper.deleteForPermission(id);
        roleMapper.deleteForEmployee(id);
        roleMapper.deleteForMenu(id);
    }

    @Override
    public Role get(Long id) {
        return roleMapper.get(id);
    }

    @Override
    public PageResult query(QueryObject qo) {
        Integer totalCount=roleMapper.getCount(qo);
        if(totalCount<1){
            return PageResult.EMPTY;
        }
        List<Role> list=roleMapper.list(qo);
        return new PageResult(qo.getCurrentPage(),qo.getPageSize(),list,totalCount);
    }

    @Override
    public List<Role> getAllRole() {
        return roleMapper.getAllRole();
    }

    @Override
    public List<String> selectMyExpression(Long id) {
        return roleMapper.selectMyExpression(id);
    }

    @Override
    public void batchDelete(List<Long> ids) {
        roleMapper.batchDelete(ids);
    }

    @Override
    public List<String> selectByUserId(Long id) {
        return roleMapper.selectByUserId(id);
    }
}