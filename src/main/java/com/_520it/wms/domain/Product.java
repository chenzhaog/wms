package com._520it.wms.domain;

import com.alibaba.fastjson.JSON;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@Getter@Setter
public class Product extends BaseDomain{

    private String name;

    private String sn;

    private BigDecimal costPrice;

    private BigDecimal salePrice;

    private String imagePath;

    private String intro;

    private Brand brand;
    public String getSmallImagePath(){
        if(imagePath==null){
            return null;
        }
        String[] s = imagePath.split("\\.");

        return s[0]+"_small."+s[1];
    }
    public String getProductJson(){
        Map<String,Object> map=new HashMap<>();
        map.put("id",super.id);
        map.put("name",name);
        map.put("costPrice",costPrice);
        map.put("salePrice",salePrice);
        map.put("brand",brand.getName());
        return JSON.toJSONString(map);
    }

}