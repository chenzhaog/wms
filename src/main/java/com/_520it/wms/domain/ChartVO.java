package com._520it.wms.domain;

import java.math.BigDecimal;

public class ChartVO {
    private String groupType;
    private BigDecimal totalNumber;
    private BigDecimal totalAmount;
}
