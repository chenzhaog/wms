package com._520it.wms.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Brand extends BaseDomain {
private Long id;
    private String name;

    private String sn;

}