package com._520it.wms.util;

import java.util.Calendar;
import java.util.Date;

public class DateUtil {
    public DateUtil(){}
    public static Date zeroDate(Date date){
        Calendar calendar=Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY,23);
        calendar.set(Calendar.MINUTE,59);
        calendar.set(Calendar.SECOND,59);
        return calendar.getTime();
    }
}
