package com._520it.wms.util;

import com._520it.wms.domain.Employee;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.shiro.SecurityUtils;

import java.util.List;

public class UserContext extends ActionSupport{
    private UserContext(){

    }
    public static void putContextUser(Employee employee){
        ActionContext.getContext().getSession().put("user_in_session",employee);
    }
    public static void putContextExpression(List<String> expressions){
        ActionContext.getContext().getSession().put("expression_in_session",expressions);
    }
    public static Employee getSessionUser(){
        return (Employee) SecurityUtils.getSubject().getPrincipal();
    }
    public static List<String> getSessionExpression(){
        return (List<String>) ActionContext.getContext().getSession().get("expression_in_session");
    }
}
