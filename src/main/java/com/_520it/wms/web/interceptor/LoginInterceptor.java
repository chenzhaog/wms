package com._520it.wms.web.interceptor;

import com._520it.wms.util.UserContext;
import com._520it.wms.domain.Employee;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

public class LoginInterceptor extends AbstractInterceptor {
    @Override
    public String intercept(ActionInvocation actionInvocation) throws Exception {
        Employee user = UserContext.getSessionUser();
        if(user==null){
            return Action.ERROR;
        }
        return actionInvocation.invoke();
    }
}
