package com._520it.wms.web.interceptor;

import com._520it.wms.util.RequiredPermission;
import com._520it.wms.util.UserContext;
import com._520it.wms.domain.Employee;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

import java.lang.reflect.Method;
import java.util.List;

public class SafetyCheckInterceptor extends AbstractInterceptor {
    @Override
    public String intercept(ActionInvocation actionInvocation) throws Exception {
        List<String> expression = UserContext.getSessionExpression();
        System.out.println(expression);
        Employee user = UserContext.getSessionUser();
        Class<?> name = actionInvocation.getProxy().getAction().getClass();
        String method = actionInvocation.getProxy().getMethod();
        if(user.isAdmin()){
            return actionInvocation.invoke();
        }
        Method m = name.getMethod(method);
        RequiredPermission annotation = m.getAnnotation(RequiredPermission.class);
        if(annotation==null){
            return actionInvocation.invoke();
        }
        if(expression.contains(name.getName()+":"+method)){
            return actionInvocation.invoke();
        }
        return "nopermission";
    }
}
