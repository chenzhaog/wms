package com._520it.wms.web.action;

import com._520it.wms.domain.ProductStock;
import com._520it.wms.query.ProductStockQueryObject;
import com._520it.wms.service.IBrandService;
import com._520it.wms.service.IDepotService;
import com._520it.wms.service.IProductStockService;
import lombok.Getter;
import lombok.Setter;

public class ProductStockAction extends BaseAction {
    @Setter
    private IProductStockService productStockService;
    @Getter
    private ProductStockQueryObject qo = new ProductStockQueryObject();
    @Getter
    private ProductStock productStock = new ProductStock();
    @Setter
    private IDepotService depotService;
    @Setter
    private IBrandService brandService;
    @Override
    public String execute() throws Exception {
        putContext("pageResult", productStockService.query(qo));
        putContext("depots",depotService.selectAll());
        putContext("brands",brandService.selectAll());
        return LIST;
    }
}