package com._520it.wms.web.action;

import com._520it.wms.util.RequiredPermission;
import com._520it.wms.domain.MenuJson;
import com._520it.wms.domain.SystemMenu;
import com._520it.wms.query.SystemMenuQueryObject;
import com._520it.wms.service.ISystemMenuService;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

public class SystemMenuAction extends BaseAction {
    @Setter
    private ISystemMenuService systemMenuService;
    @Getter
    private SystemMenuQueryObject qo = new SystemMenuQueryObject();
    @Getter
    private SystemMenu systemMenu = new SystemMenu();
    @Setter
    private String sn;
    @Override
    public String execute() throws Exception {
        putContext("pageResult", systemMenuService.query(qo));
        if(qo.getParentId()>0){
            List<SystemMenu> parentMenus= systemMenuService.getAllParent(qo.getParentId());
            putContext("parentMenus",parentMenus);
        }
        return LIST;
    }

    @RequiredPermission("SystemMenu删除")
    public String delete() throws Exception {
        try {
            systemMenuService.deleteByPrimaryKey(systemMenu.getId());
            putJson(DELETE_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            putJson(DELETE_FAIL);
        }
        return NONE;
    }

    @RequiredPermission("SystemMenu修改")
    public String input() {
        if (systemMenu.getId() != null) {
            systemMenu = systemMenuService.selectByPrimaryKey(this.systemMenu.getId());
        }
        if (qo.getParentId() > 0) {
            String parentName = systemMenuService.selectByPrimaryKey(qo.getParentId()).getName();
            putContext("parentName",parentName);
        }else{
            putContext("parentName","根目录");
        }
        return INPUT;
    }

    @RequiredPermission("SystemMenu修改")
    public String saveOrUpdate() {
        try {
            if(systemMenu.getParent().getId()==-1){
                systemMenu.getParent().setId(null);
            }
            if (systemMenu.getId() != null) {
                systemMenuService.updateByPrimaryKey(systemMenu);
                super.addActionMessage("修改成功");
            } else {
                System.out.println(systemMenu.getParent().getId());
                systemMenuService.insert(systemMenu);
                super.addActionMessage("增加成功");
            }
            qo.setParentId(systemMenu.getParent().getId());
        } catch (Exception e) {
            e.printStackTrace();
            super.addActionError("出错了");
        }
        return SUCCESS;
    }
    public String queryMenusBySn()throws Exception{
        List<MenuJson> menus=systemMenuService.queryMenusBySn(sn);
        putJson(menus);
        return NONE;
    }
}