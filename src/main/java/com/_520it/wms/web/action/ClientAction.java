package com._520it.wms.web.action;

import com._520it.wms.util.RequiredPermission;
import com._520it.wms.domain.Client;
import com._520it.wms.query.ClientQueryObject;
import com._520it.wms.service.IClientService;
import lombok.Getter;
import lombok.Setter;

public class ClientAction extends BaseAction {
    @Setter
    private IClientService clientService;
    @Getter
    private ClientQueryObject qo = new ClientQueryObject();
    @Getter
    private Client client = new Client();

    @Override
    public String execute() throws Exception {
        putContext("pageResult", clientService.query(qo));
        return LIST;
    }

    @RequiredPermission("Client删除")
    public String delete() throws Exception {
        try {
            clientService.deleteByPrimaryKey(client.getId());
            putJson(DELETE_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            putJson(DELETE_FAIL);
        }
        return NONE;
    }

    @RequiredPermission("Client修改")
    public String input() {
        if (client.getId() != null) {
            client = clientService.selectByPrimaryKey(this.client.getId());
        }
        return INPUT;
    }

    @RequiredPermission("Client修改")
    public String saveOrUpdate() {
        try {
            if (client.getId() != null) {
                clientService.updateByPrimaryKey(client);
                super.addActionMessage("修改成功");
            } else {
                clientService.insert(client);
                super.addActionMessage("增加成功");
            }
        } catch (Exception e) {
            e.printStackTrace();
            super.addActionError("出错了");
        }
        return SUCCESS;
    }
}