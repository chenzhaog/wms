package com._520it.wms.web.action;

import com._520it.wms.util.RequiredPermission;
import com._520it.wms.domain.Permission;
import com._520it.wms.domain.Role;
import com._520it.wms.domain.SystemMenu;
import com._520it.wms.query.QueryObject;
import com._520it.wms.service.IPermissionService;
import com._520it.wms.service.IRoleService;
import com._520it.wms.service.ISystemMenuService;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

public class RoleAction extends BaseAction {
    @Getter@Setter
    private List<Long> ids=new ArrayList<>();
    @Setter
    private IRoleService roleService;
    @Getter
    private QueryObject qo=new QueryObject();
    @Getter
    private Role role=new Role();
    @Setter
    private IPermissionService permissionService;
    @Setter
    private ISystemMenuService systemMenuService;
    @Override
    public String execute() throws Exception {
        putContext("pageResult",roleService.query(qo));
        return LIST;
    }
    @RequiredPermission("角色删除")
    public String delete()throws Exception{
        try {
            roleService.delete(role.getId());
            putJson(DELETE_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            putJson(DELETE_FAIL);
        }
        return NONE;
    }
    @RequiredPermission("角色编辑")
    public String input(){
        if(role.getId()!=null){
            role = roleService.get(this.role.getId());
            if(role.getPermissions()==null){
                role.setPermissions(new ArrayList<Permission>());
            }
            putContext("role",role);
        }
        List<SystemMenu> menus=systemMenuService.selectAllChild();
        putContext("menus",menus);
        putContext("permissions",permissionService.selectAllPermission());
        return INPUT;
    }
    @RequiredPermission("角色修改")
    public String saveOrUpdate(){
        try {
            if(role.getId()!=null){
                roleService.update(role);
                super.addActionMessage("修改成功");
            }else{
                roleService.save(role);
                super.addActionMessage("保存成功");
            }
        } catch (Exception e) {
            e.printStackTrace();
            super.addActionError("出现错误");
        }
        return SUCCESS;
    }
    public String batchDelete() throws Exception {
        try {
            if (ids.size() > 0) {
                roleService.batchDelete(ids);
            putJson("删除成功");
            }
        } catch (Exception e) {
            putJson("删除失败");
            e.printStackTrace();
        }
        return NONE;
    }
}
