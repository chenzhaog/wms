package com._520it.wms.web.action;

import com._520it.wms.query.ChartQueryObject;
import com._520it.wms.service.IBrandService;
import com._520it.wms.service.IChartService;
import com._520it.wms.service.IClientService;
import com._520it.wms.service.ISupplierService;
import com.alibaba.fastjson.JSON;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ChartAction extends BaseAction {
    @Setter
    private IChartService chartService;
    @Setter
    private IBrandService brandService;
    @Setter
    private ISupplierService supplierService;
    @Getter
    private ChartQueryObject oqo = new ChartQueryObject();
    @Setter
    private IClientService clientService;

    public String orderChart() {
        putContext("list", chartService.list(oqo));
        putContext("types", ChartQueryObject.map);
        putContext("brands", brandService.selectAll());
        putContext("suppliers", supplierService.selectAll());
        return "orderChart";
    }

    public String saleChart() {
        putContext("list", chartService.saleList(oqo));
        putContext("types", ChartQueryObject.saleMap);
        putContext("brands", brandService.selectAll());
        putContext("clients", clientService.selectAll());
        return "saleChart";
    }

    public String saleChartByPie() {
        List<Map<String, Object>> maps = chartService.saleList(oqo);
        List<String> types = new ArrayList<>();
        List<BigDecimal> amounts = new ArrayList<>();
        for (Map<String, Object> map : maps) {
            String type = (String) map.get("groupType");
            types.add(JSON.toJSONString(type));
            BigDecimal amount = (BigDecimal) map.get("totalAmount");
            amounts.add(amount);
        }
        putContext("types", types);
        putContext("amounts", amounts);
        return "saleChartByPie";
    }

    public String saleChartByLine() {
        List<Map<String, Object>> maps = chartService.saleList(oqo);
        List<String> types = new ArrayList<>();
        List<Map<String, Object>> amounts = new ArrayList<>();
        BigDecimal max=BigDecimal.ZERO;
        for (Map<String, Object> map : maps) {
            Map<String, Object> mapJson = new HashMap<>();
            String type = (String) map.get("groupType");
            types.add(JSON.toJSONString(type));
            BigDecimal amount = (BigDecimal) map.get("totalAmount");
            mapJson.put("value", amount);
            mapJson.put("name", type);
            amounts.add(mapJson);
            if(amount.compareTo(max)>-1){
                max=amount;
            }
        }
        putContext("max",max);
        putContext("types", types);
        putContext("amounts", JSON.toJSONString(amounts));
        return "saleChartByLine";
    }
}