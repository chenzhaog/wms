package com._520it.wms.web.action;

import com._520it.wms.util.RequiredPermission;
import com._520it.wms.domain.Supplier;
import com._520it.wms.query.SupplierQueryObject;
import com._520it.wms.service.ISupplierService;
import lombok.Getter;
import lombok.Setter;

public class SupplierAction extends BaseAction {
    @Setter
    private ISupplierService supplierService;
    @Getter
    private SupplierQueryObject qo = new SupplierQueryObject();
    @Getter
    private Supplier supplier = new Supplier();

    @Override
    public String execute() throws Exception {
        putContext("pageResult", supplierService.query(qo));
        return LIST;
    }

    @RequiredPermission("Supplier删除")
    public String delete() throws Exception {
        try {
            supplierService.deleteByPrimaryKey(supplier.getId());
            putJson(DELETE_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            putJson(DELETE_FAIL);
        }
        return NONE;
    }

    @RequiredPermission("Supplier修改")
    public String input() {
        if (supplier.getId() != null) {
            supplier = supplierService.selectByPrimaryKey(this.supplier.getId());
        }
        return INPUT;
    }

    @RequiredPermission("Supplier修改")
    public String saveOrUpdate() {
        try {
            if (supplier.getId() != null) {
                supplierService.updateByPrimaryKey(supplier);
                super.addActionMessage("修改成功");
            } else {
                supplierService.insert(supplier);
                super.addActionMessage("增加成功");
            }
        } catch (Exception e) {
            e.printStackTrace();
            super.addActionError("出错了");
        }
        return SUCCESS;
    }
}