package com._520it.wms.web.action;

import com._520it.wms.util.RequiredPermission;
import com._520it.wms.domain.StockOutcomeBill;
import com._520it.wms.query.StockOutcomeBillQueryObject;
import com._520it.wms.service.IClientService;
import com._520it.wms.service.IDepotService;
import com._520it.wms.service.IStockOutcomeBillService;
import lombok.Getter;
import lombok.Setter;

public class StockOutcomeBillAction extends BaseAction {
    @Setter
    private IStockOutcomeBillService stockOutcomeBillService;
    @Getter
    private StockOutcomeBillQueryObject qo = new StockOutcomeBillQueryObject();
    @Getter
    private StockOutcomeBill stockOutcomeBill = new StockOutcomeBill();
    @Setter
    private IDepotService depotService;
    @Setter
    private IClientService clientService;
    @Override
    public String execute() throws Exception {
        putContext("pageResult", stockOutcomeBillService.query(qo));
        putContext("depots",depotService.selectAll());
        putContext("clients",clientService.selectAll());
        return LIST;
    }

    @RequiredPermission("StockOutcomeBill删除")
    public String delete() throws Exception {
        try {
            stockOutcomeBillService.deleteByPrimaryKey(stockOutcomeBill.getId());

            putJson(DELETE_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            putJson(DELETE_FAIL);
        }
        return NONE;
    }

    @RequiredPermission("StockOutcomeBill修改")
    public String input() {
        if (stockOutcomeBill.getId() != null) {
            stockOutcomeBill = stockOutcomeBillService.selectByPrimaryKey(this.stockOutcomeBill.getId());
        }
        putContext("depots",depotService.selectAll());
        putContext("clients",clientService.selectAll());
        return INPUT;
    }

    @RequiredPermission("StockOutcomeBill修改")
    public String saveOrUpdate() {
        try {
            if (stockOutcomeBill.getId() != null) {
                stockOutcomeBillService.updateByPrimaryKey(stockOutcomeBill);
                super.addActionMessage("修改成功");
            } else {
                stockOutcomeBillService.insert(stockOutcomeBill);
                super.addActionMessage("增加成功");
            }
        } catch (Exception e) {
            e.printStackTrace();
            super.addActionError("出错了");
        }
        return SUCCESS;
    }
    public String audit()throws Exception{
        try {
            stockOutcomeBillService.updateAudit(stockOutcomeBill);
            putJson("审成功");
        } catch (Exception e) {
            e.printStackTrace();
            putJson(e.getMessage());
        }
        return NONE;
    }
    public String show(){
        if (stockOutcomeBill.getId() != null) {
            stockOutcomeBill = stockOutcomeBillService.selectByPrimaryKey(this.stockOutcomeBill.getId());
        }
        putContext("depots",depotService.selectAll());
        putContext("clients",clientService.selectAll());
        return "show";
    }
}