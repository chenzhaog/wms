package com._520it.wms.web.action;

import com._520it.wms.util.RequiredPermission;
import com._520it.wms.domain.Brand;
import com._520it.wms.query.BrandQueryObject;
import com._520it.wms.service.IBrandService;
import lombok.Getter;
import lombok.Setter;

public class BrandAction extends BaseAction {
    @Setter
    private IBrandService brandService;
    @Getter
    private BrandQueryObject qo=new BrandQueryObject();
    @Getter
    private Brand brand=new Brand();
    @Override
    public String execute() throws Exception {
        putContext("pageResult",brandService.query(qo));
        return LIST;
    }
    @RequiredPermission("Brand删除")
    public String delete()throws Exception{
        try {
            brandService.deleteByPrimaryKey(brand.getId());
            putJson(DELETE_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            putJson(DELETE_FAIL);
        }
        return SUCCESS;
    }
    @RequiredPermission("Brand修改")
    public String input(){
        if(brand.getId()!=null){
            brand = brandService.selectByPrimaryKey(this.brand.getId());
        }
        return INPUT;
    }
    @RequiredPermission("Brand修改")
    public String saveOrUpdate(){
        try {
            if(brand.getId()!=null){
                brandService.updateByPrimaryKey(brand);
                super.addActionMessage("修改成功");
            }else{
                brandService.insert(brand);
                super.addActionMessage("修改失败");
            }
        } catch (Exception e) {
            e.printStackTrace();
            super.addActionError("出错了");
        }
        return SUCCESS;
    }
}