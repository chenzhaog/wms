package com._520it.wms.web.action;

import com._520it.wms.util.RequiredPermission;
import com._520it.wms.domain.OrderBill;
import com._520it.wms.query.OrderBillQueryObject;
import com._520it.wms.service.IOrderBillService;
import com._520it.wms.service.ISupplierService;
import lombok.Getter;
import lombok.Setter;

public class OrderBillAction extends BaseAction {
    @Setter
    private IOrderBillService orderBillService;
    @Getter
    private OrderBillQueryObject qo = new OrderBillQueryObject();
    @Getter
    private OrderBill orderBill = new OrderBill();
    @Setter
    private ISupplierService supplierService;

    @Override
    public String execute() throws Exception {
        putContext("pageResult", orderBillService.query(qo));
        putContext("suppliers",supplierService.selectAll());
        return LIST;
    }

    @RequiredPermission("OrderBill删除")
    public String delete() throws Exception {
        try {
            orderBillService.deleteByPrimaryKey(orderBill.getId());
            putJson(DELETE_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            putJson(DELETE_FAIL);
        }
        return NONE;
    }

    @RequiredPermission("OrderBill修改")
    public String input() {
        if (orderBill.getId() != null) {
            orderBill = orderBillService.selectByPrimaryKey(this.orderBill.getId());
        }
        putContext("suppliers",supplierService.selectAll());
        return INPUT;
    }

    @RequiredPermission("OrderBill修改")
    public String saveOrUpdate() {
        try {
            if (orderBill.getId() != null) {
                orderBillService.updateByPrimaryKey(orderBill);
                super.addActionMessage("修改成功");
            } else {
                orderBillService.insert(orderBill);
                super.addActionMessage("增加成功");
            }
        } catch (Exception e) {
            e.printStackTrace();
            super.addActionError("出错了");
        }
        return SUCCESS;
    }
    public String audit(){
        orderBillService.updateAudit(orderBill);
        return SUCCESS;
    }
    public String show(){
        if (orderBill.getId() != null) {
            orderBill = orderBillService.selectByPrimaryKey(this.orderBill.getId());
        }
        putContext("suppliers",supplierService.selectAll());
        return "show";
    }
}