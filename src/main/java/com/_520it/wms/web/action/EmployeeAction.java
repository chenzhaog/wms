package com._520it.wms.web.action;

import com._520it.wms.domain.Employee;
import com._520it.wms.query.EmployeeQueryObject;
import com._520it.wms.query.QueryObject;
import com._520it.wms.service.IDepartmentService;
import com._520it.wms.service.IEmployeeService;
import com._520it.wms.service.IRoleService;
import com._520it.wms.util.RequiredPermission;
import com.opensymphony.xwork2.interceptor.annotations.InputConfig;
import lombok.Getter;
import lombok.Setter;
import org.apache.shiro.authz.annotation.RequiresPermissions;

import java.util.ArrayList;
import java.util.List;

public class EmployeeAction extends BaseAction {
    @Setter
    private IEmployeeService employeeService;
    @Getter
    private QueryObject qo = new EmployeeQueryObject();
    @Getter
    private Employee employee = new Employee();
    @Setter
    private IDepartmentService departmentService;
    @Setter
    private IRoleService roleService;
    @Getter
    @Setter
    private List<Long> ids = new ArrayList<>();
    @Getter
    @Setter
    private Integer isOk = null;

    @Override
    @RequiresPermissions("employee:execute")
    @InputConfig(methodName = "input")
    public String execute() throws Exception {
        try {
            putContext("pageResult", employeeService.query(qo));
            putContext("depts", departmentService.getAllName());
            System.out.println(employeeService.query(qo));
//            int error=1/0;
        } catch (Exception e) {
            super.addActionError("出现异常");
            e.printStackTrace();
        }
        return LIST;
    }

    @RequiredPermission("用户删除")
    @RequiresPermissions("employee:delete")
    public String delete() throws Exception {
        try {
            employeeService.delete(employee.getId());
            putJson("删除成功");
        } catch (Exception e) {
            e.printStackTrace();
            putJson("删除失败");
        }
        return NONE;
    }

    @RequiredPermission("用户编辑")
    @RequiresPermissions(value = {"employee:input","用户访问"})
    public String input() {
        if (getActionErrors().size() > 1) {
            String s = new ArrayList<>(getActionErrors()).get(1);
            employee.setId(Long.valueOf(s));
        }
        if (employee.getId() != null) {
            employee = employeeService.get(this.employee.getId());
            putContext("employee", employee);
        }
        System.out.println(getActionErrors());
        putContext("depts", departmentService.getAllName());
        putContext("roles", roleService.getAllRole());
        return INPUT;
    }

    @RequiredPermission("用户修改")
    @RequiresPermissions(value = {"employee:saveOrUpdate","用户访问"})
    public String saveOrUpdate() {
        if (employee.getId() != null) {
            try {
                employeeService.update(employee);
                super.addActionMessage("更新成功");
            } catch (Exception e) {
                super.addActionError("出错了");
                super.addActionError(employee.getId().toString());
                e.printStackTrace();
            }

        } else {
            try {
                employeeService.save(employee);
                super.addActionMessage("增加成功");
            } catch (Exception e) {
                super.addActionError("出错了");
                e.printStackTrace();
            }
        }
        return SUCCESS;
    }

    public String batchDelete() throws Exception {
        try {
            if (ids.size() > 0) {
                employeeService.batchDelete(ids);
            }
            putJson("删除成功");
        } catch (Exception e) {
            putJson("删除失败");
            e.printStackTrace();
        }
        return NONE;
    }
}
