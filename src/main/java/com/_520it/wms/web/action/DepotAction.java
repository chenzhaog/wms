package com._520it.wms.web.action;

import com._520it.wms.util.RequiredPermission;
import com._520it.wms.domain.Depot;
import com._520it.wms.query.DepotQueryObject;
import com._520it.wms.service.IDepotService;
import lombok.Getter;
import lombok.Setter;

public class DepotAction extends BaseAction {
    @Setter
    private IDepotService depotService;
    @Getter
    private DepotQueryObject qo = new DepotQueryObject();
    @Getter
    private Depot depot = new Depot();

    @Override
    public String execute() throws Exception {
        putContext("pageResult", depotService.query(qo));
        return LIST;
    }

    @RequiredPermission("Depot删除")
    public String delete() throws Exception {
        try {
            depotService.deleteByPrimaryKey(depot.getId());
            putJson(DELETE_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            putJson(DELETE_FAIL);
        }
        return NONE;
    }

    @RequiredPermission("Depot修改")
    public String input() {
        if (depot.getId() != null) {
            depot = depotService.selectByPrimaryKey(this.depot.getId());
        }
        return INPUT;
    }

    @RequiredPermission("Depot修改")
    public String saveOrUpdate() {
        try {
            if (depot.getId() != null) {
                depotService.updateByPrimaryKey(depot);
                super.addActionMessage("修改成功");
            } else {
                depotService.insert(depot);
                super.addActionMessage("增加成功");
            }
        } catch (Exception e) {
            e.printStackTrace();
            super.addActionError("出错了");
        }
        return SUCCESS;
    }
}