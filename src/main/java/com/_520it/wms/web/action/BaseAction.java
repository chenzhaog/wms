package com._520it.wms.web.action;


import com.alibaba.fastjson.JSON;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.ServletActionContext;

public class BaseAction extends ActionSupport{
    public static final String DELETE_SUCCESS="删除成功";
    public static final String DELETE_FAIL="删除失败";
    public static final String LIST="list";
    public void putContext(String key,Object value){
        ActionContext.getContext().put(key,value);
    }
    protected void putJson(Object obj)throws Exception{
        String s = JSON.toJSONString(obj);
        ServletActionContext.getResponse().setContentType("text/json;charset=utf-8");
        ServletActionContext.getResponse().getWriter().println(s);
    }

}
