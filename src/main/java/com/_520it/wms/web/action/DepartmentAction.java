package com._520it.wms.web.action;

import com._520it.wms.util.RequiredPermission;
import com._520it.wms.domain.Department;
import com._520it.wms.query.DepartmentQueryObject;
import com._520it.wms.query.QueryObject;
import com._520it.wms.service.IDepartmentService;
import lombok.Getter;
import lombok.Setter;

public class DepartmentAction extends BaseAction {
    @Setter
    private IDepartmentService departmentService;
    @Getter
    private QueryObject qo=new DepartmentQueryObject();
    @Getter
    private Department department=new Department();
    @Override
    public String execute() throws Exception {
        putContext("pageResult",departmentService.query(qo));
        return LIST;
    }
    @RequiredPermission("部门删除")
    public String delete() throws Exception{
        try {
            departmentService.delete(department.getId());
            putJson(DELETE_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            putJson(DELETE_FAIL);
        }
        return NONE;
    }
    @RequiredPermission("部门编辑")
    public String input(){
        if(department.getId()!=null){
            department = departmentService.get(this.department.getId());
        }
        return INPUT;
    }
    @RequiredPermission("部门修改")
    public String saveOrUpdate(){
        try {
            if(department.getId()!=null){
                departmentService.update(department);
                super.addActionMessage("更改成功");
            }else{
                departmentService.save(department);
                super.addActionMessage("保存成功");
            }
        } catch (Exception e) {
            e.printStackTrace();
            super.addActionError("操作失败");
        }
        return SUCCESS;
    }
}
