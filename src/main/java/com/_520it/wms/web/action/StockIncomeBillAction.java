package com._520it.wms.web.action;

import com._520it.wms.util.RequiredPermission;
import com._520it.wms.domain.StockIncomeBill;
import com._520it.wms.query.StockIncomeBillQueryObject;
import com._520it.wms.service.IDepotService;
import com._520it.wms.service.IStockIncomeBillService;
import lombok.Getter;
import lombok.Setter;

public class StockIncomeBillAction extends BaseAction {
    @Setter
    private IStockIncomeBillService stockIncomeBillService;
    @Getter
    private StockIncomeBillQueryObject qo = new StockIncomeBillQueryObject();
    @Getter
    private StockIncomeBill stockIncomeBill = new StockIncomeBill();
    @Setter
    private IDepotService depotService;

    @Override
    public String execute() throws Exception {
        putContext("pageResult", stockIncomeBillService.query(qo));
        putContext("depots",depotService.selectAll());
        return LIST;
    }

    @RequiredPermission("StockIncomeBill删除")
    public String delete() throws Exception {
        try {
            stockIncomeBillService.deleteByPrimaryKey(stockIncomeBill.getId());
            putJson(DELETE_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            putJson(DELETE_FAIL);
        }
        return NONE;
    }

    @RequiredPermission("StockIncomeBill修改")
    public String input() {
        if (stockIncomeBill.getId() != null) {
            stockIncomeBill = stockIncomeBillService.selectByPrimaryKey(this.stockIncomeBill.getId());
        }
        putContext("depots",depotService.selectAll());
        return INPUT;
    }

    @RequiredPermission("StockIncomeBill修改")
    public String saveOrUpdate() {
        try {
            if (stockIncomeBill.getId() != null) {
                stockIncomeBillService.updateByPrimaryKey(stockIncomeBill);
                super.addActionMessage("修改成功");
            } else {
                stockIncomeBillService.insert(stockIncomeBill);
                super.addActionMessage("增加成功");
            }
        } catch (Exception e) {
            e.printStackTrace();
            super.addActionError("出错了");
        }
        return SUCCESS;
    }
    public String audit() throws Exception{
        try {
            stockIncomeBillService.updateAudit(stockIncomeBill);
            putJson("审核成功");
        } catch (Exception e) {
            e.printStackTrace();
            putJson(e.getMessage());
        }
        return NONE;
    }
    public String show(){
        if (stockIncomeBill.getId() != null) {
            stockIncomeBill = stockIncomeBillService.selectByPrimaryKey(this.stockIncomeBill.getId());
        }
        putContext("depots",depotService.selectAll());
        return "show";
    }
}