package com._520it.wms.web.action;

import com._520it.wms.util.RequiredPermission;
import com._520it.wms.domain.Permission;
import com._520it.wms.query.QueryObject;
import com._520it.wms.service.IPermissionService;
import lombok.Getter;
import lombok.Setter;

public class PermissionAction extends BaseAction {
    @Setter
    private IPermissionService permissionService;
    @Getter
    private Permission permission=new Permission();
    @Getter
    private QueryObject qo=new QueryObject();
    @Override
    public String execute() throws Exception {
        putContext("pageResult",permissionService.query(qo));
        return LIST;
    }
    @RequiredPermission("权限删除")
    public String delete()throws Exception{
        try {
            permissionService.delete(permission.getId());
            putJson(DELETE_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            putJson(DELETE_FAIL);
        }
        return NONE;
    }
    @RequiredPermission("权限加载")
    public String reload() throws Exception{
        try {
            permissionService.reload();
            putJson("加载成功");
        } catch (Exception e) {
            e.printStackTrace();
            putJson("加载失败");
        }
        return NONE;
    }
}
