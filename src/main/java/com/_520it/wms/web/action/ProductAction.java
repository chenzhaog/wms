package com._520it.wms.web.action;

import com._520it.wms.util.FileUploadUtil;
import com._520it.wms.util.RequiredPermission;
import com._520it.wms.domain.Product;
import com._520it.wms.query.ProductQueryObject;
import com._520it.wms.service.IBrandService;
import com._520it.wms.service.IProductService;
import lombok.Getter;
import lombok.Setter;

import java.io.File;

public class ProductAction extends BaseAction {
    @Setter
    private IProductService productService;
    @Getter
    private ProductQueryObject qo = new ProductQueryObject();
    @Getter
    private Product product = new Product();
    @Setter
    private IBrandService brandService;
    @Setter
    private File pic;
    @Setter
    private String picFileName;
    @Override
    public String execute() throws Exception {
        putContext("pageResult", productService.query(qo));
        putContext("brands", brandService.selectAll());
        return LIST;
    }

    @RequiredPermission("Product删除")
    public String delete() throws Exception {
        try {
            FileUploadUtil.deleteFile(product.getImagePath());
            productService.deleteByPrimaryKey(product.getId());
            putJson(DELETE_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            putJson(DELETE_FAIL);
        }
        return NONE;
    }

    @RequiredPermission("Product修改")
    public String input() {
        if (product.getId() != null) {
            product = productService.selectByPrimaryKey(this.product.getId());
        }
        putContext("brands", brandService.selectAll());
        return INPUT;
    }

    @RequiredPermission("Product修改")
    public String saveOrUpdate() {
        try {
            if (product.getId() != null) {
                if(product.getImagePath()!=null&&pic!=null){
                    FileUploadUtil.deleteFile(product.getImagePath());
                    String imagePath = FileUploadUtil.uploadFile(pic, picFileName);
                    product.setImagePath(imagePath);
                }
                productService.updateByPrimaryKey(product);
                super.addActionMessage("修改成功");
            } else {
                if(pic!=null){
                    String imagePath = FileUploadUtil.uploadFile(pic, picFileName);
                    product.setImagePath(imagePath);
                }
                productService.insert(product);
                super.addActionMessage("增加成功");
            }
        } catch (Exception e) {
            e.printStackTrace();
            super.addActionError("出错了");
        }
        return SUCCESS;
    }
    public String selectProduct(){
        putContext("pageResult", productService.query(qo));
        putContext("brands", brandService.selectAll());
        return "selectProduct";
    }
}