package com._520it.wms.query;

public class ProductQueryObject extends QueryObject{
    private String keyword;
    private Long brandId=-1L;
    public String getKeyword() {
        return ifNull(keyword);
    }
    public Long getBrandId(){
        if(brandId<0){
            return null;
        }
        return brandId;
    }
}
