package com._520it.wms.query;

import lombok.Getter;
import lombok.Setter;

@Getter@Setter
public class SystemMenuQueryObject extends QueryObject{
    private Long parentId=-1L;
    public Long getParentId(){
        if(parentId==null){
            return -1L;
        }
        return parentId;
    }
}
