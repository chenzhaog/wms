package com._520it.wms.query;

import com._520it.wms.util.DateUtil;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
@Getter@Setter
public class ChartQueryObject extends QueryObject{
    public static final Map<String,String> map=new LinkedHashMap<>();
    public static final Map<String,String> saleMap=new LinkedHashMap<>();
    static {
        map.put("e.name","订贷人员");
        map.put("p.name","物品名称");
        map.put("s.name","供应商");
        map.put("b.name","物品品牌");
        map.put("date_format(bill.vdate,'%Y-%m')","订贷日期(月)");
        map.put("date_format(bill.vdate,'%Y-%m-%d')","订贷日期(日)");
        saleMap.put("e.name","销售人员");
        saleMap.put("p.name","物品名称");
        saleMap.put("c.name","客户");
        saleMap.put("b.name","物品品牌");
        saleMap.put("date_format(sa.vdate,'%Y-%m')","订贷日期(月)");
        saleMap.put("date_format(sa.vdate,'%Y-%m-%d')","订贷日期(日)");

    }
    private String groupType="e.name";
    private Date beginDate;
    private Date endDate;
    private Long supplierId=-1L;
    private Long brandId=-1L;
    private Long clientId=-1L;
    private String keyword;
    public Date getEndDate(){
        if(endDate!=null){
            return DateUtil.zeroDate(endDate);
        }
        return null;
    }
}
