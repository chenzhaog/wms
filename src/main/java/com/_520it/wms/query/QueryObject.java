package com._520it.wms.query;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

@Getter@Setter
public class QueryObject {
    private Integer currentPage=1;
    private Integer pageSize=3;
    private Integer start;
    public Integer getStart(){
        return (currentPage-1)*pageSize;
    }
    protected String ifNull(String s){
        if(!StringUtils.isNotBlank(s)){
            return null;
        }
        return s;
    }
}
