package com._520it.wms.query;

import lombok.Getter;
import lombok.Setter;

import java.util.Collections;
import java.util.List;

@Getter@Setter
public class PageResult {
    private Integer currentPage;
    private Integer pageSize;
    private List<?> list;
    private Integer totalCount;
    private Integer endPage;
    private Integer prevPage;
    private Integer nextPage;
    public static final PageResult EMPTY=new PageResult(1,1, Collections.EMPTY_LIST,0);
    public PageResult(Integer currentPage, Integer pageSize, List<?> list, Integer totalCount) {
        this.currentPage = currentPage;
        this.pageSize = pageSize;
        this.list = list;
        this.totalCount = totalCount;
        endPage=totalCount%pageSize==0?totalCount/pageSize:totalCount/pageSize+1;
        prevPage=currentPage-1<1?1:currentPage-1;
        nextPage=currentPage+1>endPage?endPage:currentPage+1;
    }
}
