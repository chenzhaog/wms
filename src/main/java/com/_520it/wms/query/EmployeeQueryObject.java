package com._520it.wms.query;

import lombok.Getter;
import lombok.Setter;

@Getter@Setter
public class EmployeeQueryObject extends QueryObject {
    private String keyword;
    private Long deptId;
    public String getKeyword() {
        return ifNull(keyword);
    }
    public Long getDeptId(){
        if(deptId==null||deptId<0){
            return null;
        }
        return deptId;
    }
}
