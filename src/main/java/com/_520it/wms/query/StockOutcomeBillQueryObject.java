package com._520it.wms.query;

import com._520it.wms.util.DateUtil;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
@Getter@Setter
public class StockOutcomeBillQueryObject extends QueryObject {
    private Date beginDate;
    private Date endDate;
    private Long clientId=-1L;
    private Long depotId=-1L;
    private Integer status=-1;
    public Long getDepotId(){
        if(depotId<0){
            return null;
        }
        return depotId;
    }
    public Long getClientId(){
        if(clientId<0){
            return null;
        }
        return clientId;
    }
    public Integer getStatus(){
        if(status<0){
            return null;
        }
        return status;
    }
    public Date getEndDate(){
        if(endDate!=null){
            return DateUtil.zeroDate(endDate);
        }
        return null;
    }
}
