jQuery.ajaxSettings.traditional = true;
$(function () {
    //新增页面
    $(".btn_input").click(function () {
        var url = $(this).data("url");
        location.href = url;
    });
    //分页
    $(".btn_page").click(function () {
        var pageNo = $(this).data("page") || $("input[name='qo.currentPage']").val();
        $(":input[name='qo.currentPage']").val(pageNo);
        $("#searchForm").submit();
    });
    //更改页面容量
    $(".select_size").change(function () {
        $(":input[name='qo.currentPage']").val(1);
        $("#searchForm").submit();
    });
    //批量删除
    $(".btn_batch").click(function () {
        if ($(".acb:checked").size() < 1) {
            showDialog("", "请选择删除的列");
            return;
        }
            var url = $(this).data("deleteurl");
        console.log(url);
        showDialog("", "是否要删除", function () {
            var bat = $.map($(".acb:checked"), function (ele) {
                return $(ele).data("batch");
            });
            if (bat) {
                $.get(url, {ids: bat}, function () {
                    showDialog("", "删除成功", function () {
                        location.reload();
                    })
                });
            }
        }, true);
    });
    //清除选择
    $(":input:checked").prop("checked",false);
    //删除
    $(".a_del").click(function () {
        var url = $(this).data("durl");
        $.artDialog({
            title: "删除",
            content: "是否删除",
            ok: function () {
                $.get(url,function (data) {
                    showDialog("",data,function () {

                    location.reload();
                    });
                });
            },
            cancel: true,
            icon:"face-smile"
        });
    });
    $("#all").change(function () {
        $("tbody input:checkbox").prop("checked",this.checked);
    });
});
//提示框
function showDialog(title, content, ok, cancel) {
    $.dialog({
        title: title || "温馨提示",
        content: content,
        ok: ok || true,
        cancel: cancel || false,
        icon:"face-smile"
    });
}