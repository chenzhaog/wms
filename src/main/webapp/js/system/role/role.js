//权限
$(function () {
   $("#selectAll").click(function () {
       $(".all_permission option").appendTo($(".selected_permission"));
   });
   $("#deselectAll").click(function () {
       $(".all_permission").append($(".selected_permission option"));
   });
    $("#select").click(function () {
        $(".all_permission option:selected").appendTo($(".selected_permission"));
    });
    $("#deselect").click(function () {
        $(".all_permission").append($(".selected_permission option:selected"));
    });
    var arr=$.map($(".selected_permission option"),function (item,index) {
        return $(item).val();
    });
    $.each($(".all_permission option"),function (index,item) {
        console.log(item);
        if($.inArray($(item).val(),arr)>-1){
            $(item).remove();
        }
    });
    //菜单
    //-------------------------------------//
    $("#mselectAll").click(function () {
        $(".all_menu option").appendTo($(".selected_menu"));
    });
    $("#mdeselectAll").click(function () {
        $(".all_menu").append($(".selected_menu option"));
    });
    $("#mselect").click(function () {
        $(".all_menu option:selected").appendTo($(".selected_menu"));
    });
    $("#mdeselect").click(function () {
        $(".all_menu").append($(".selected_menu option:selected"));
    });
    var arr=$.map($(".selected_menu option"),function (item,index) {
        return $(item).val();
    });
    $.each($(".all_menu option"),function (index,item) {
        console.log(item);
        if($.inArray($(item).val(),arr)>-1){
            $(item).remove();
        }
    });
    //---------------------------------------------//
    $("#editForm").submit(function () {
        $(".selected_permission option").prop("selected",true);
        $(".selected_menu option").prop("selected",true);
    });
});