$(function () {
   $(".btn_reload").click(function () {
       var url = $(this).data("url");
       showDialog("","是否加载,加载会有点慢",function () {
       $.get(url,function (data) {
           showDialog("",data,function () {
                location.reload();
           });
       })
       },true);
   });
});