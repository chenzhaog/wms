//加载当前日期
function loadDate() {
    var time = new Date();
    var myYear = time.getFullYear();
    var myMonth = time.getMonth() + 1;
    var myDay = time.getDate();
    if (myMonth < 10) {
        myMonth = "0" + myMonth;
    }
    // $("#day_day").html(myYear + "." + myMonth + "." + myDay);
    setInterval(function () {
        $("#day_day").html(new Date().toLocaleString());

    }, 1000)
}

/**
 * 隐藏或者显示侧边栏
 *
 */
function switchSysBar(flag) {
    var side = $('#side');
    var left_menu_cnt = $('#left_menu_cnt');
    if (flag == true) { // flag==true
        left_menu_cnt.show(500, 'linear');
        side.css({
            width: '280px'
        });
        $('#top_nav').css({
            width: '77%',
            left: '304px'
        });
        $('#main').css({
            left: '280px'
        });
    } else {
        if (left_menu_cnt.is(":visible")) {
            left_menu_cnt.hide(10, 'linear');
            side.css({
                width: '60px'
            });
            $('#top_nav').css({
                width: '100%',
                left: '60px',
                'padding-left': '28px'
            });
            $('#main').css({
                left: '60px'
            });
            $("#show_hide_btn").find('img').attr('src',
                '/images/common/nav_show.png');
        } else {
            left_menu_cnt.show(500, 'linear');
            side.css({
                width: '280px'
            });
            $('#top_nav').css({
                width: '77%',
                left: '304px',
                'padding-left': '0px'
            });
            $('#main').css({
                left: '280px'
            });
            $("#show_hide_btn").find('img').attr('src',
                '/images/common/nav_hide.png');
        }
    }
}

// =====================================
$(function () {
    // 加载日期
    loadDate();

});

//============================================
//菜单显示
//============================================
$(function () {
    $("#dleft_tab1 a").click(function () {
        var action = $(this).data("url");
        //根据id获取到iframe
        $("#rightMain").prop("src", action);
    })
})
$(function () {
    // 显示隐藏侧边栏
    $("#show_hide_btn").click(function () {
        switchSysBar();
    });
});
//============================================
//菜单面板项切换
//============================================
$(function () {
    menu("business");
    $("#TabPage2 li").click(function () {
        $("#TabPage2 li").each(function (index, item) {
            $(item).find("img").prop("src", "/images/common/" + (index + 1) + ".jpg");
            $(item).removeClass("selected");
        });
        var index = $(this).index();
        $(this).find("img").prop("src", "/images/common/" + (index + 1) + "_hover.jpg");
        $(this).addClass("selected");
        $("#nav_module img").prop("src", "/images/common/module_" + (index + 1) + ".png")
        var rootName = $(this).data("rootmenu");
        // $("#here_area").html("当前位置");
        menu(rootName);
    });
});
//zTree
function menu(menuName) {
    var setting = {
        data: {
            simpleData: {
                enable: true
            }
        },
        callback: {
            onClick: function (event, treeId, treeNode) {
                console.log(treeNode.getParentNode());
                if (treeNode.getParentNode()) {
                    $("#here_area").html("当前位置：" + treeNode.getParentNode().name + "&nbsp;>&nbsp;" + treeNode.name);
                }
            }
        },
        async: {
            enable: true,
            url: "systemMenu_queryMenusBySn.action",
            autoParam: ["sn=sn"]
        }
    };
    var system = [
        {id: 1, pId: 0, name: "系统模块", isParent: true, sn: "system"}
    ];
    var business = [
        {id: 1, pId: 0, name: "业务模块", isParent: true, sn: "business"}
    ];
    var chart = [
        {id: 1, pId: 0, name: "报表模块", isParent: true, sn: "chart"}
    ];
    var zNodes = {"system": system, "business": business, "chart": chart};
    $.fn.zTree.init($("#dleft_tab1"), setting, zNodes[menuName]);
}

