$(function () {
    $("#editForm").validate({
        rules:{
            'employee.name':{required:true,minlength:2},
            'employee.password':{required:true,minlength:2},
            repassword:{required:true,equalTo:"#password"},
            'employee.email':{email:true},
            'employee.age':{required:true,range:[18,60]}
        },
        messages:{
            'employee.name':{required:"用户名必填",minlength:"长度最小为2"},
            'employee.password':{required:"密码名必填",minlength:"长度最小为2"},
            repassword:{equalTo:"必须和密码一致"},
            'employee.email':{email:"格式不对"},
            'employee.age':{required:"年龄必填",range:"年龄必须在18-60岁之间"}
        }
    });
});
$(function () {
    $("#selectAll").click(function () {
        $(".all_permission option").appendTo($(".selected_permission"));
    });
    $("#deselectAll").click(function () {
        $(".all_permission").append($(".selected_permission option"));
    });
    $("#select").click(function () {
        $(".all_permission option:selected").appendTo($(".selected_permission"));
    });
    $("#deselect").click(function () {
        $(".all_permission").append($(".selected_permission option:selected"));
    });
    var arr=$.map($(".selected_permission option"),function (item,index) {
        return $(item).val();
    });
    $.each($(".all_permission option"),function (index,item) {
        console.log(item);
        if($.inArray($(item).val(),arr)>-1){
            $(item).remove();
        }
    });
    $("#editForm").submit(function () {
        $(".selected_permission option").prop("selected",true);
    });
});