<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="/style/basic_layout.css" rel="stylesheet" type="text/css">
    <link href="/style/common_style.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="/js/jquery/jquery.js"></script>
    <script type="text/javascript" src="/plugins/artDialog/jquery.artDialog.js?skin=blue"></script>
    <script type="text/javascript" src="/js/commonAll.js"></script>
    <title>PSS-账户管理</title>
    <style>
        .alt td {
            background: black !important;
        }
    </style>
    <%@ include file="/WEB-INF/views/common/common-msg.jsp"%>
</head>
<body>
<s:form id="searchForm" action="employee" method="post">
    <div id="container">
        <div class="ui_content">
            <div class="ui_text_indent">
                <div id="box_border">
                    <div id="box_top">搜索</div>
                    <div id="box_center">
                        姓名/邮箱
                        <s:textfield name="qo.keyword" cssClass="ui_input_txt02"/>
                        所属部门
                        <s:select name="qo.deptId" list="#depts" listKey="id" listValue="name" headerKey="-1"
                                  headerValue="全部" class="ui_select01"/>
                    </div>
                    <div id="box_bottom">
                        <input type="button" value="查询" class="ui_input_btn01 btn_page" data-page="1"/>
                        <input type="button" value="新增" class="ui_input_btn01 btn_input"
                               data-url="<s:url namespace="/" action="employee_input"/> "/>
                        <input type="button" value="批量删除" class="ui_input_btn01 btn_batch" data-deleteurl="<s:url action="employee_batchDelete"/>">
                    </div>
                </div>
            </div>
        </div>
        <div class="ui_content">
            <div class="ui_tb">
                <table class="table" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
                    <tr>
                        <th width="30"><input type="checkbox" id="all"/></th>
                        <th>编号</th>
                        <th>用户名</th>
                        <th>EMAIL</th>
                        <th>年龄</th>
                        <th>所属部门</th>
                        <th>操作</th>
                        <th></th>
                    </tr>
                    <tbody>
                    <s:iterator value="#pageResult.list" status="num">
                        <tr>
                            <td><input type="checkbox" name="IDCheck" class="acb" data-batch="<s:property value="id"/>"/></td>
                            <td><s:property value="#num.count"/></td>
                            <td><s:property value="name"/></td>
                            <td><s:property value="email"/></td>
                            <td><s:property value="age"/></td>
                            <td><s:property value="dept.name"/></td>
                            <td>
                                <s:a action="employee_input">
                                    <s:param name="employee.id" value="id"/>
                                    编辑
                                </s:a>
                                <s:url action="employee_delete" var="delLink" >
                                    <s:param name="employee.id" value="id"/>
                                </s:url>
                                <a href="javascript:;" class="a_del" data-durl="<s:property value="#delLink"/>">删除</a>
                            </td>
                        </tr>
                    </s:iterator>
                    </tbody>
                </table>
                <s:debug></s:debug>
            </div>
            <%@ include file="/WEB-INF/views/common/page.jsp"%>
        </div>
    </div>
</s:form>
<s:hidden name="isOk" id="ok"/>
</body>
</html>
