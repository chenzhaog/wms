<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html>
<head>
    <title>信息管理系统</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link href="/style/basic_layout.css" rel="stylesheet" type="text/css"/>
    <link href="/style/common_style.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="/js/jquery/jquery.js"></script>
    <script type="text/javascript" src="/plugins/artDialog/jquery.artDialog.js?skin=blue"></script>
    <script type="text/javascript" src="/plugins/My97DatePicker/WdatePicker.js"></script>
    <script type="text/javascript" src="/plugins/artDialog/iframeTools.js"></script>
    <script>
        $(function(){
            //设置日期控件
            $("[name='stockOutcomeBill.vdate']").addClass("Wdate").click(function(){
                WdatePicker();
            });
            //绑定事件选择产品信息
            $("#edit_table_body").on("click",".searchproduct",function(){
                //获取到当前行
                var currentTr = $(this).closest("tr");
                //通过argDialog弹出产品选择界面
                var url="/product_selectProduct.action";
                $.dialog.open(url,{
                    "id":"selectProduct",
                    "title":"产品选择",
                    "width":880,
                    "height":680,
                    "close":function(){
                        //关闭之前执行的操作
                        //获取传递过来的数据
                        var productJson = $.dialog.data("productJson");
                        if(productJson){
                            //获取到当前行的对应的后代元素,并赋值操作
                            currentTr.find("[tag=name]").val(productJson.name);
                            currentTr.find("[tag=pid]").val(productJson.id);
                            currentTr.find("[tag=brand]").html(productJson.brand);
                            currentTr.find("[tag=salePrice]").val(productJson.salePrice);

                        }
                    }
                });
            }).on("change","[tag=salePrice],[tag=number]",function(){
                //获取到当前行
                var currentTr = $(this).closest("tr");
               var amount = currentTr.find("[tag=salePrice]").val()* currentTr.find("[tag=number]").val();
                //赋值操作
                currentTr.find("[tag=amount]").html(amount.toFixed(2));
            }).on("click",".removeItem",function(){
                //获取到当前行
                var currentTr = $(this).closest("tr");
                //判断当前表单里面是否只有一条明细
                if($("#edit_table_body tr").size()>1){
                    currentTr.remove();//删除当前行
                }else{
                    //清空数据
                    currentTr.find("[tag=name],[tag=pid],[tag=salePrice],[tag=number],[tag=remark]").val("");
                    currentTr.find("[tag=brand],[tag=amount]").html("");
                }
            });
            //添加明细事件
            $(".appendRow").click(function(){
                //简单拷贝一个tr元素
                var cloneTr=$("#edit_table_body tr:first").clone();
                //清空拷贝的数据
                cloneTr.find("[tag=name],[tag=pid],[tag=salePrice],[tag=number],[tag=remark]").val("");
                cloneTr.find("[tag=brand],[tag=amount]").html("");
                //添加到tbody中
                cloneTr.appendTo($("#edit_table_body"));
            });
            //表单的提交事件
            $("#editForm").submit(function(){
                //遍历tbody中的tr元素
                $.each($("#edit_table_body tr"),function(index,item){
                    //找到每一行的input元素,设置其对应的相关的name属性的值, 与index有关系
                    //需要提交到后台的数据: pid,salePrice,number,remark
                    $(item).find("[tag=pid]").prop("name","stockOutcomeBill.items["+index+"].product.id");
                    $(item).find("[tag=salePrice]").prop("name","stockOutcomeBill.items["+index+"].salePrice");
                    $(item).find("[tag=number]").prop("name","stockOutcomeBill.items["+index+"].number");
                    $(item).find("[tag=remark]").prop("name","stockOutcomeBill.items["+index+"].remark");
                });
            });
        })
    </script>
</head>
<body>
<!-- =============================================== -->
<%@include file="/WEB-INF/views/common/common-msg.jsp" %>
<s:form name="editForm" namespace="/" action="stockOutcomeBill_saveOrUpdate" method="post" id="editForm">
    <div id="container">
        <div id="nav_links">
            <span style="color: #1A5CC6;">采购出库单编辑</span>
            <div id="page_close">
                <a>
                    <img src="images/common/page_close.png" width="20" height="20" style="vertical-align: text-top;"/>
                </a>
            </div>
        </div>
        <div class="ui_content">
            <table cellspacing="0" cellpadding="0" width="100%" align="left" border="0">
                <s:hidden name="stockOutcomeBill.id"></s:hidden>
                <tr>
                    <td class="ui_text_rt" width="140">出库单编号</td>
                    <td class="ui_text_lt">
                        <s:textfield name="stockOutcomeBill.sn" cssClass="ui_input_txt02"></s:textfield>
                    </td>
                </tr>
                <tr>
                    <td class="ui_text_rt" width="140">仓库</td>
                    <td class="ui_text_lt">
                        <s:select list="#depots" listKey="id"
                                  listValue="name" name="stockOutcomeBill.depot.id" cssClass="ui_select03"/>
                    </td>
                </tr>
                <tr>
                    <td class="ui_text_rt" width="140">客户</td>
                    <td class="ui_text_lt">
                        <s:select list="#clients" listKey="id"
                                  listValue="name" name="stockOutcomeBill.client.id" cssClass="ui_select03"/>
                    </td>
                </tr>
                <tr>
                    <td class="ui_text_rt" width="140">业务时间</td>
                    <td class="ui_text_lt">
                        <s:date name="stockOutcomeBill.vdate" format="yyyy-MM-dd" var="vdate"/>
                        <s:textfield name="stockOutcomeBill.vdate" cssClass="ui_input_txt02" value="%{#vdate}"></s:textfield>
                    </td>
                </tr>
                <tr>
                    <td class="ui_text_rt" width="140">出库单明细</td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <input type="button" value="添加明细" class="ui_input_btn01 appendRow"/>
                        <table class="edit_table" cellspacing="0" cellpadding="0" border="0" style="width: auto">
                            <thead>
                            <tr>
                                <th width="10"></th>
                                <th width="200">货品</th>
                                <th width="120">品牌</th>
                                <th width="80">价格</th>
                                <th width="80">数量</th>
                                <th width="80">金额小计</th>
                                <th width="150">备注</th>
                                <th width="60"></th>
                            </tr>
                            </thead>
                            <tbody id="edit_table_body">
                            <%--通过单据id判断是编辑操作还是新增操作--%>
                            <s:if test="stockOutcomeBill.id==null"> <%--新增操作--%>
                                <tr>
                                    <td></td>
                                    <td>
                                        <s:textfield disabled="true" readonly="true" cssClass="ui_input_txt02"
                                                     tag="name"/>
                                        <img src="/images/common/search.png" class="searchproduct"/>
                                        <s:hidden name="stockOutcomeBill.items[0].product.id" tag="pid"/>
                                    </td>
                                    <td><span tag="brand"></span></td>
                                    <td><s:textfield tag="salePrice" name="stockOutcomeBill.items[0].salePrice"
                                                     cssClass="ui_input_txt02"/></td>
                                    <td><s:textfield tag="number" name="stockOutcomeBill.items[0].number"
                                                     cssClass="ui_input_txt02"/></td>
                                    <td><span tag="amount"></span></td>
                                    <td><s:textfield tag="remark" name="stockOutcomeBill.items[0].remark"
                                                     cssClass="ui_input_txt02"/></td>
                                    <td>
                                        <a href="javascript:;" class="removeItem">删除明细</a>
                                    </td>
                                </tr>
                            </s:if>
                            <s:else><%--编辑操作--%>
                                <s:iterator value="stockOutcomeBill.items">
                                    <tr>
                                        <td></td>
                                        <td>
                                            <s:textfield disabled="true" name="product.name" readonly="true" cssClass="ui_input_txt02"
                                                         tag="name"/>
                                            <img src="/images/common/search.png" class="searchproduct"/>
                                            <s:hidden name="product.id" tag="pid"/>
                                        </td>
                                        <td><span tag="brand"><s:property value="product.brand.name"/></span></td>
                                        <td><s:textfield tag="salePrice" name="salePrice"
                                                         cssClass="ui_input_txt02"/></td>
                                        <td><s:textfield tag="number" name="number"
                                                         cssClass="ui_input_txt02"/></td>
                                        <td><span tag="amount"><s:property value="amount"/></span></td>
                                        <td><s:textfield tag="remark" name="remark"
                                                         cssClass="ui_input_txt02"/></td>
                                        <td>
                                            <a href="javascript:;" class="removeItem">删除明细</a>
                                        </td>
                                    </tr>
                                </s:iterator>
                            </s:else>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="ui_text_lt">
                        &nbsp;<input type="submit" value="确定保存" class="ui_input_btn01"/>
                        &nbsp;<input id="cancelbutton" type="button" value="重置" class="ui_input_btn01"/>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</s:form>
</body>
</html>