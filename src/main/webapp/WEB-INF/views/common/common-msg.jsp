<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<script>
    $(function () {
        var data="<s:property value="actionMessages[0]"/>";
        if(data){
            $.artDialog({
                title: "温馨提示",
                content: data,
                ok: true
            })
        }
    })
    $(function () {
        var data="<s:property value="actionErrors[0]"/>";
        if(data){
            $.artDialog({
                title: "温馨提示",
                content: data,
                ok: true
            })
        }
    })
</script>
